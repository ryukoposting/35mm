/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "main.h"
#include "ui.h"
#include "container.h"
#include <ctype.h>
#include <string.h>

#define MARQUEE_PAUSE_TICKS 8
#define MARQUEE_SPACING 6


static void fill_marquee(struct timer *tmr, char *window, usize winlen)
{ /* this function assumes that the size of buffer `window` is at least winlen+1 */
  usize const namelen = strnlen(tmr->name, TIMER_NAME_MAXLEN);

  if (namelen < winlen) {
    snprintf(window, winlen+1, "%s", tmr->name);
    return;
  }

  unsigned const t = get_ticks() - tmr->marquee_state;
  usize const nstates = namelen + MARQUEE_SPACING;

  usize cur_state = t % (nstates + MARQUEE_PAUSE_TICKS);
  if (cur_state < MARQUEE_PAUSE_TICKS)
    cur_state = 0;
  else
    cur_state -= MARQUEE_PAUSE_TICKS;

  usize i;
  for (i = 0; i < winlen; ++i) {
    usize pos = i + cur_state;
    if (pos < namelen)
      window[i] = tmr->name[pos];
    else if (pos < namelen + MARQUEE_SPACING)
      window[i] = ' ';
    else
      window[i] = tmr->name[pos - (namelen + MARQUEE_SPACING)];
  }
  window[i] = '\0';
}


void show_timerlist()
{
  static char tmbuf[128] = {};

  int col, row;
  getmaxyx(stdscr, row, col);

  int r = 1;

  int timeri = 0;
  llist_eachit(&g_timerlist, {
    struct timer *timerp = container_of(IT, struct timer, llnode);
    unsigned t = ((read_timer(timerp) + (TICKS_PER_SEC - 1)) / TICKS_PER_SEC);
    move(r, 3);

    printw("%02d:%02d  ", t / 60, t % 60);

    usize const max_noscroll = (unsigned)col / 2 - 10;
    usize const tmwidth = min(sizeof(tmbuf)-1u, max_noscroll);

    if (timeri != get_sel_tmr()) {
      fill_marquee(timerp, tmbuf, tmwidth);
    } else {
      timerp->marquee_state = get_ticks();
      snprintf(tmbuf, tmwidth, "%s", timerp->name);
    }

    printw("%-24s", tmbuf);

    if (timerp->state == TRUNNING && col > 72) {
      if (g_config.unicode) {
        switch ((get_ticks() / 3) % 4) {
        case 0: mvaddwstr(r, 1, L"\u25f4"); break;
        case 1: mvaddwstr(r, 1, L"\u25f7"); break;
        case 2: mvaddwstr(r, 1, L"\u25f6"); break;
        case 3: mvaddwstr(r, 1, L"\u25f5"); break;
        }
      } else {
        mvaddstr(r, 1, "R");
      }
    } else if (timerp->state == TFINISHED && col > 72) {
      if (g_config.unicode) {
        mvaddwstr(r, 1, L"\u25cf");
      } else {
        mvaddstr(r, 1, "F");
      }
    }

    if (timeri == get_sel_tmr()) {
      move(r, 1);
      if (!get_timeredit_active()) {
        if (g_config.colors)
          attron(COLOR_PAIR(C_MAGENTA));

        attron(A_BOLD);
      }
      if (g_config.unicode)
        addwstr(L"\u2192");
      else
        addstr("*");
      if (!get_timeredit_active()) {
        if (g_config.colors)
          attroff(COLOR_PAIR(C_MAGENTA));

        attroff(A_BOLD);
      }
    }
    r++;
    timeri++;
  });

  if (timeri == 0) {
    mvprintw(row / 2, 1, "[%c] - new timer", g_config.keymap[KNEWTIMER]);
  }
}

int feed_timerlist(int x)
{
  int timeri = 0;
  struct timer *timerp = NULL;
  llist_eachit(&g_timerlist, {
    if (timeri == get_sel_tmr())
      timerp = container_of(IT, struct timer, llnode);
    timeri++;
  });

  if (tolower(x) == g_config.keymap[KSTARTTIMER]) {
    start_timer(timerp);
    return 0;

  } else if (tolower(x) == g_config.keymap[KDELTIMER]) {
    delete_timer(timerp);
    return 0;

  } else if (tolower(x) == g_config.keymap[KNEWTIMER]) {
    new_timer("New Timer", 10);
    return 0;

  } else if (tolower(x) == g_config.keymap[KRESETTIMER]) {
    stop_timer(timerp);
    return 0;

  } else if (tolower(x) == g_config.keymap[KPAUSETIMER]) {
    pause_timer(timerp);
    return 0;

  } else if (tolower(x) == g_config.keymap[KMOVEUP]) {
    move_timer(-1, timerp);
    set_sel_tmr(get_sel_tmr() - 1);
    return 0;

  } else if (tolower(x) == g_config.keymap[KMOVEDOWN]) {
    move_timer(1, timerp);
    set_sel_tmr(get_sel_tmr() + 1);
    return 0;

  } else if (timerp && x == KEY_RIGHT) {
    set_timeredit_active(true);
    return 0;

  } else if (x == KEY_UP) {
    set_sel_tmr(get_sel_tmr() - 1);
    return 0;

  } else if (x == KEY_DOWN) {
    set_sel_tmr(get_sel_tmr() + 1);
    return 0;
  }

  return 1;
}
