/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "ui.h"
#include <stdlib.h>

void show_welcome()
{
  int col, row;
  getmaxyx(stdscr, row, col);

  mvchgat(8, 8, -1, A_BOLD, C_WHITE, NULL);

  col /= 2;
  row /= 2;
  row -= 3;
  attron(A_BOLD);
  if (g_config.colors)
    attron(COLOR_PAIR(C_MAGENTA));
  cprintw(row, col, "Welcome to 35mm");
  ++row;
  if (g_config.colors)
    attron(COLOR_PAIR(C_WHITE));
  cprintw(row, col, "`A tiny development timer in the terminal'");
  attroff(A_BOLD);
  ++row;
  ++row;
  cprintw(row, col, "Copyright (c) Evan Perry Grove");
  ++row;
  cprintw(row, col, "Version 0.0.1, November 2021");
  ++row;
  ++row;
  cprintwf(row, col, "[%c] - exit", g_config.keymap[KEXIT]);
  ++row;
  cprintwf(row, col, "[%c] - new timer", g_config.keymap[KNEWTIMER]);

  if (g_config.colors)
    attroff(COLOR_PAIR(C_WHITE));
}

int feed_welcome(int x)
{
  unused(x);
  return 1;
}

