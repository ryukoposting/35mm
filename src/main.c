/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>

#include "main.h"
#include "ui.h"
#include "mem/mallocator.h"
#include "mem/alrc.h"

#include "parser/ini.h"
#include "stream/arrstream.h"

struct mallocator g_mallocator;
struct alrc g_alrc;
struct config g_config;
struct llist g_timerlist;

static void init_mem()
{
  int ret;

  ret = mallocator_init(&g_mallocator);
  if (ret) {
    perror("mallocator_init");
    abort();
  }

  ret = alrc_init(&g_mallocator.allocator, &g_alrc);
  if (ret) {
    perror("alrc_init");
    abort();
  }

  ret = llist_init(&g_timerlist);
  if (ret) {
    perror("llist_init");
    abort();
  }
}

void die(char const *msg, ...)
{
  if (curscr)
    endwin();

  va_list ap;
  va_start(ap, msg);
  vfprintf(stderr, msg, ap);
  va_end(ap);

  exit(EXIT_FAILURE);
}

static unsigned t_ms()
{
  struct timeval t;
  gettimeofday(&t, NULL);
  return (1000u * t.tv_sec) + (t.tv_usec / 1000u);
}

static void help()
{
  static char const helpmsg[] =
  "\n35mm (--help|--version) [--colors] [--unicode] [--init]\n\n"
  "usage:\n"
  "  --help               show this message.\n"
  "  --version            show version info.\n"
  "  --colors=(on|off)    enable/disable CLI colors. this setting\n"
  "                       overrides the config file.\n"
  "  --unicode=(on|off)   enable/disable usage of unicode characters.\n"
  "                       This setting overrides the config file.\n"
  "  --init               create the default config file, if the config\n"
  "                       file does not already exist.\n"
  "\n"
  "configuration:\n"
  "  35mm can be configured using an INI file. By default, this file is\n"
  "  located at $HOME/.config/35mm.ini -- but, if you want 35mm to read\n"
  "  from a different config file, you can set the CONFIGFILE_35MM\n"
  "  environment variable.\n"
  "\n"
  "  Refer to the manpage for more information about 35mm config files.\n"
  "\n"
  ;

  printf("%s", helpmsg);
}

static void init()
{
  static char const default_config_ini[] =
  "colors=on\n"
  "unicode=on\n"
  "\n"
  "[keymap]\n"
  "exit=q\n"
  "new=n\n"
  "delete=d\n"
  "start=\\n\n"
  "reset=r\n"
  "pause=p\n"
  "moveup=k\n"
  "movedown=j\n";

  if (g_config.cfgpath) {
    fprintf(stderr, "init given, but a config file already exists! I will not overwrite it.\n");
    exit(EXIT_FAILURE);
  }

  char *p = default_cfgpath();
  int fd = open(p, O_WRONLY | O_CREAT, 0777);
  if (fd < 0) {
    perror("failed to create new config file");
    alrc_put(p);
    exit(EXIT_FAILURE);
  }

  isize r, n = 0;
  do {
    n += r = write(fd, default_config_ini, sizeof(default_config_ini)-1);
  } while (n < (isize)sizeof(default_config_ini)-1 && r > 0);

  if (r < 0) {
    perror("failed while writing config file");
    alrc_put(p);
    close(fd);
    exit(EXIT_FAILURE);
  }

  close(fd);
  g_config.cfgpath = p;
  fprintf(stderr, "new config file created at %s\n", p);
}

static void version()
{
  printf("35mm %s\n", VERSION);
#if GIT_DIRTY
  printf("Git: %s (dirty)\n", GIT_HASH);
#else
  printf("Git: %s\n", GIT_HASH);
#endif
  printf("License: MPL-2.0: Mozilla Public License Version 2.0\n");
  printf("         <https://www.mozilla.org/media/MPL/2.0/index.txt>\n");
}

int main(int argc, char **argv)
{
  int const cycle_ms = 8;

  setlocale(LC_ALL, "C.UTF-8");

  init_mem();

  load_config(argc, argv);

  if (g_config.help) {
    help();
    exit(EXIT_SUCCESS);
  }

  if (g_config.version) {
    version();
    exit(EXIT_SUCCESS);
  }

  if (g_config.init) {
    init();
  }

	initscr();			/* Start curses mode 		*/

  raw();
  keypad(stdscr, TRUE);
  noecho();
  timeout(cycle_ms);

  curs_set(0);

	start_color();			/* Start color functionality	*/
  init_pair(C_RED, COLOR_RED, COLOR_BLACK);
  init_pair(C_CYAN, COLOR_CYAN, COLOR_BLACK);
  init_pair(C_WHITE, COLOR_WHITE, COLOR_BLACK);
  init_pair(C_MAGENTA, COLOR_MAGENTA, COLOR_BLACK);

  int ret = 0;
  int next_check = 0;
  unsigned t = t_ms();
  unsigned t2 = 0;
  do {
    erase();
    show();
    int ch = getch();
    if (ch != ERR)
      ret = feed(ch);

    if (next_check)
      next_check -= 1;
    else {
      t2 = t_ms();
      if (t2 - TICK_LENGTH_MSEC >= t) {
        t = t2;
        tick();
        next_check = (TICK_LENGTH_MSEC / cycle_ms) - 1;
      }
    }
  } while (ret >= 0);


  endwin();

	return 0;
}
