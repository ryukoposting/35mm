/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <ncurses.h>
#include <stdlib.h>
#include <ctype.h>
#include "ui.h"
#include "container.h"

static uint64 g_uimask = (1 << UI_TOOLBAR) | (1 << UI_WELCOME);

static unsigned ticks = 0;

void show()
{
  int col, row;
  getmaxyx(stdscr, row, col);
  if (row < 16) {
    die("error: terminal must be at least 16 characters tall.\n");
  }
  if (col < 70) {
    die("error: terminal must be at least 70 characters wide.\n");
  }

  for (unsigned i = 0; i < sizeof(g_uimask) * 8; ++i) {
    if (g_uimask & (1 << i)) {
      switch (i) {
      case UI_WELCOME: show_welcome(); break;
      case UI_TOOLBAR: show_toolbar(); break;
      case UI_TIMEREDIT: show_timeredit(); break;
      case UI_TIMERLIST: show_timerlist(); break;
      }
    }
  }

  refresh();
}

int feed(int x)
{
  int ret = 1;

  for (unsigned i = 0; (i < sizeof(g_uimask) * 8) && ret > 0; ++i) {
    if (g_uimask & (1 << i)) {
      switch (i) {
      case UI_WELCOME: ret = feed_welcome(x); break;
      case UI_TOOLBAR: ret = feed_toolbar(x); break;
      case UI_TIMEREDIT: ret = feed_timeredit(x); break;
      case UI_TIMERLIST: ret = feed_timerlist(x); break;
      }
    }
  }

  return ret;
}

void tick()
{
  ++ticks;

  llist_eachit(&g_timerlist, {
    struct timer *timerp = container_of(IT, struct timer, llnode);
    update_timer(timerp);
  });
}

unsigned get_ticks()
{
  return ticks;
}

wchar_t k2wc(int x)
{
  if (g_config.unicode) {
    switch (x) {
    case KEY_RIGHT: return L'\u2192';
    case KEY_LEFT: return L'\u2190';
    case KEY_UP: return L'\u2191';
    case KEY_DOWN: return L'\u2193';
    case '\n': return L'\u00bb';
    default: return x;
    }
  } else {
    switch (x) {
    case KEY_RIGHT: return '>';
    case KEY_LEFT: return '<';
    case KEY_UP: return '^';
    case KEY_DOWN: return 'V';
    case '\n': return 'R';
    default: return tolower(x);
    }
  }
}

bool kallowed(int x)
{
  return isalnum(x)
    || x == KEY_RIGHT
    || x == KEY_LEFT
    || x == KEY_UP
    || x == KEY_DOWN
    || x == '\n';
}

void ui_enable(enum ui_elems elem)
{
  g_uimask |= 1u << elem;
}

void ui_disable(enum ui_elems elem)
{
  g_uimask &= ~(1u << elem);
}

bool ui_is_enabled(enum ui_elems elem)
{
  return 0 != (g_uimask & (1 << elem));
}
