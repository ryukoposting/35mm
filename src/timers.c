/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "main.h"
#include "ui.h"
#include "container.h"
#include "macros.h"
#include <string.h>
#include <stdlib.h>
#include <ncurses.h>
#include <limits.h>

static void destroy(void *p)
{
  struct timer *timer = (struct timer*)p;
  llist_remove(&timer->llnode, &g_timerlist);
  alrc_put(timer->name);
}

int new_timer(char const *name, unsigned preset_sec)
{
  struct timer *timer = alrc_alloc(sizeof(*timer), destroy, &g_alrc);
  if (!timer)
    return -1;

  int ret = llnode_init(&timer->llnode);
  if (ret)
    return ret;

  timer->name = alrc_alloc(TIMER_NAME_MAXLEN + 1, NULL, &g_alrc);
  if (!timer->name)
    return -1;
  usize ncpy = strnlen(name, TIMER_NAME_MAXLEN);
  strncpy(timer->name, name, ncpy);
  timer->name[ncpy] = '\0';
  timer->name[TIMER_NAME_MAXLEN] = '\0';
  timer->preset = preset_sec * TICKS_PER_SEC;
  timer->start = 0;
  timer->paused_at = 0;
  timer->state = TSTOPPED;

  ret = llist_push_back(&timer->llnode, &g_timerlist);
  return ret;
}

int delete_timer(struct timer *timer)
{
  return alrc_put(timer);
}

int start_timer(struct timer *timer)
{
  if (!timer)
    return -1;
  if (timer->state == TSTOPPED) {
    timer->start = get_ticks();
    timer->state = TRUNNING;
  } else if (timer->state == TPAUSED) {
    timer->start += get_ticks() - timer->paused_at;
    timer->state = TRUNNING;
  }
  return 0;
}

int stop_timer(struct timer *timer)
{
  if (!timer)
    return -1;
  timer->state = TSTOPPED;
  return 0;
}

int pause_timer(struct timer *timer)
{
  if (!timer)
    return -1;

  if (timer->state == TRUNNING) {
    if (get_ticks() >= timer->preset + timer->start)
      return stop_timer(timer);
    timer->paused_at = get_ticks();
    timer->state = TPAUSED;
  }

  return 0;
}

int update_timer(struct timer *timer)
{
  if (!timer)
    return -1;

  if (timer->state == TRUNNING) {
    if (get_ticks() >= timer->preset + timer->start) {
      timer->state = TFINISHED;
      beep();
    }
  }

  return 0;
}

int move_timer(int distance, struct timer *timer)
{
  int pos = -1, ntimers = 0;
  llist_eachit(&g_timerlist, {
    struct timer *tp = container_of(IT, struct timer, llnode);
    if (tp == timer)
      pos = ntimers;
    ++ntimers;
  });

  if (pos < 0)
    return -1;

  pos += distance;
  if (pos > ntimers)
    pos = ntimers;
  if (pos < 0)
    pos = 0;

  int ret;

  ret = llist_remove(&timer->llnode, &g_timerlist);
  if (ret)
    return ret;

  return llist_insert(&timer->llnode, (usize)pos, &g_timerlist);
}

unsigned read_timer(struct timer const *timer)
{
  if (!timer)
    return 0;

  switch (timer->state) {
  case TSTOPPED: return timer->preset;
  case TRUNNING: return timer->preset + timer->start - get_ticks();
  case TPAUSED: return timer->preset + timer->start - timer->paused_at;
  case TFINISHED: return 0;
  }

  return 0;
}
