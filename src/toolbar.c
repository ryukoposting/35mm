/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <ncurses.h>
#include <stdlib.h>
#include <ctype.h>
#include "ui.h"

#define addwchr(ch) {\
  wchar_t fmtbuf_[] = L"xx";\
  swprintf(fmtbuf_, sizeof(fmtbuf_), L"%lc", (ch));\
  addwstr(fmtbuf_);\
}

void show_toolbar()
{
  if (g_config.colors)
    mvchgat(0, 0, -1, A_UNDERLINE, C_CYAN, NULL);
  else
    mvchgat(0, 0, -1, A_UNDERLINE, 0, NULL);

  if (g_config.colors)
    attron(COLOR_PAIR(C_CYAN));

  attron(A_UNDERLINE);

  addstr("[");
  addwchr(k2wc(g_config.keymap[KEXIT]));
  addstr("] exit");

  if (ui_is_enabled(UI_TIMERLIST) && !get_timeredit_active()) {
    addstr("  [");
    addwchr(k2wc(g_config.keymap[KNEWTIMER]));
    addstr("] new");

    addstr("  [");
    addwchr(k2wc(g_config.keymap[KDELTIMER]));
    addstr("] del");

    addstr("  [");
    addwchr(k2wc(g_config.keymap[KSTARTTIMER]));
    addstr("] start");

    addstr("  [");
    addwchr(k2wc(g_config.keymap[KRESETTIMER]));
    addstr("] reset");

    addstr("  [");
    addwchr(k2wc(g_config.keymap[KPAUSETIMER]));
    addstr("] pause");

    // addstr("  [");
    // addwchr(k2wc(g_config.keymap[KSAVE]));
    // addstr("] save");

    addstr("  [");
    addwchr(k2wc(KEY_UP));
    addstr("/");
    addwchr(k2wc(KEY_DOWN));
    addstr("] navigate");

    addstr("  [");
    addwchr(k2wc(KEY_RIGHT));
    addstr("] edit");
  }

  if (ui_is_enabled(UI_TIMEREDIT) && get_timeredit_active()) {
    addstr("  [");
    addwchr(k2wc(KEY_UP));
    addstr("/");
    addwchr(k2wc(KEY_DOWN));
    addstr("] navigate");

    addstr("  [");
    addwchr(k2wc(KEY_LEFT));
    addstr("] return");
  }

  if (g_config.colors)
    mvchgat(1, 0, -1, 0, C_CYAN, NULL);
  else
    mvchgat(1, 0, -1, 0, 0, NULL);

  attroff(A_UNDERLINE);
  if (g_config.colors)
    attroff(COLOR_PAIR(C_CYAN));
}

int feed_toolbar(int x)
{
  if (tolower(x) == g_config.keymap[KEXIT] && !get_timeredit_active()) {
    return -1;
  } else if (tolower(x) == g_config.keymap[KNEWTIMER] && ui_is_enabled(UI_WELCOME)) {
    ui_disable(UI_WELCOME);
    ui_enable(UI_TIMERLIST);
    ui_enable(UI_TIMEREDIT);
    new_timer("New Timer", 10);
    return 0;
  } else if ((isspace(x) || isprint(x)) && ui_is_enabled(UI_WELCOME)) {
    ui_disable(UI_WELCOME);
  }

  return 1;
}

