/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "main.h"
#include "macros.h"
#include "parser/ini.h"
#include "parser/parser.h"
#include "stream/fdstream.h"
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#define CFGPATH_FMT "%s/.config/35mm.ini"


// static char *default_cfgpath();
static int set_defaults();
static int load_cfgfile();
static int parse_args(int argc, char **argv);


void load_config(int argc, char **argv)
{
  int result;

  result = set_defaults();
  if (result)
    die("Fatal: Failed to initialize g_config\n");

  result = load_cfgfile(argc, argv);
  if (result)
    die("Error: Could not validate config file path: %s\n", g_config.cfgpath);

  result = parse_args(argc, argv);
}


char *default_cfgpath()
{
  char *path = getenv("CONFIGFILE_35MM");

  if (path) {
    usize len = strlen(path);
    char *buf = alrc_alloc(len+1, NULL, &g_alrc);
    snprintf(buf, len+1, "%s", path);
    return buf;
  }
  
  char *home = getenv("HOME");
  if (!home)
    return NULL;

  int ntoalloc = snprintf(NULL, 0, CFGPATH_FMT, home);
  if (ntoalloc < 0 || ntoalloc > 1023) /* put sane limits on path size */
    return NULL;

  usize const nalloc = ntoalloc + 1;

  path = alrc_alloc(nalloc, NULL, &g_alrc);
  if (!path)
    return NULL;

  snprintf(path, nalloc, CFGPATH_FMT, home);

  /* option 1: env:CONFIGFILE_35MM */
  /* option 2: $HOME/.config/35mm.ini */
  return path;
}

static int set_defaults()
{
  g_config = (struct config) {
    .cfgpath = default_cfgpath(),
    .colors = true,
    .unicode = true,
    .keymap = {
      [KEXIT] = 'q',
      [KNEWTIMER] = 'n',
      [KDELTIMER] = 'd',
      [KSTARTTIMER] = '\n',
      [KRESETTIMER] = 'r',
      [KPAUSETIMER] = 'p',
      [KMOVEUP] = 'k',
      [KMOVEDOWN] = 'j',
      // [KSAVE] = 's',
    }
  };

  if (!g_config.cfgpath)
    return -1;
  else
    return 0;
}

static int parse_bool(bool *val, char const *s)
{
  if (!strncmp(s, "on", sizeof("on")) ||
      !strncmp(s, "true", sizeof("true")) ||
      !strncmp(s, "1", sizeof("1"))) {
    *val = true;
  } else if (!strncmp(s, "off", sizeof("off")) ||
      !strncmp(s, "false", sizeof("false")) ||
      !strncmp(s, "0", sizeof("0"))) {
    *val = false;
  } else {
    return -1;
  }

  return 0;
}

static int parse_keymap(int *val, char const *s)
{
  usize len = strnlen(s, 3);
  if (len > 2)
    return -1;

  if (len == 1) {
    char const c = tolower(s[0]);
    if (!isalnum(c))
      return -1;
    *val = c;
  } else if (!strcmp(s, "\\n")) {
    *val = '\n';
  } else {
    return -1;
  }

  return 0;
}

static void cfgini_die(struct parse_item const *item, char const *msg, ...) __attribute__((noreturn));
static void cfgini_die(struct parse_item const *item, char const *msg, ...)
{
  va_list ap;
  va_start(ap, msg);
  vfprintf(stderr, msg, ap);
  va_end(ap);
  fprintf(stderr, "Error occurred near line %d\n", item->lineinfo.line);
  die("Config file: %s\n", g_config.cfgpath);
}

static void cfgini_receive(struct parse_item const *item, struct parser const *parser)
{
  unused(parser);

  static enum {
    INIT,
    COLORS,
    UNICODE,
    KEYMAP,
    UNKNOWN_OBJ,
  } state = INIT;

  static enum keymap key = NKEYMAP;

  switch (item->type) {
  case PARSE_ERROR:
    cfgini_die(item, "Parse error: %s\n", item->msg);
    break;
  
  case PARSE_OBJ_START:
    if (!item->str)
      break; /* ignore the initial "root object" start */

    if (!strcmp(item->str, "keymap"))
      state = KEYMAP;
    else
      state = UNKNOWN_OBJ;
    break;

  case PARSE_KEY:
    if (state == INIT) {
      if (!strcmp(item->str, "colors"))
        state = COLORS;
      else if (!strcmp(item->str, "unicode"))
        state = UNICODE;

    } else if (state == KEYMAP) {
      if (!strcmp(item->str, "exit"))
        key = KEXIT;
      else if (!strcmp(item->str, "new"))
        key = KNEWTIMER;
      else if (!strcmp(item->str, "delete"))
        key = KDELTIMER;
      else if (!strcmp(item->str, "start"))
        key = KSTARTTIMER;
      else if (!strcmp(item->str, "reset"))
        key = KRESETTIMER;
      else if (!strcmp(item->str, "pause"))
        key = KPAUSETIMER;
      else if (!strcmp(item->str, "moveup"))
        key = KMOVEUP;
      else if (!strcmp(item->str, "movedown"))
        key = KMOVEDOWN;
      // else if (!strcmp(item->str, "save"))
      //   key = KSAVE;
      else {
        fprintf(stderr, "Warning: keymap '%s' was not recognized\n", item->str);
        key = NKEYMAP;
      }
    }
    break;

  case PARSE_STR:
    if (state == COLORS) {
      if (parse_bool(&g_config.colors, item->str))
        cfgini_die(item, "Error: 'colors' setting must be a boolean (on/off true/false 0/1)\n");
      state = INIT;
    } else if (state == UNICODE) {
      if (parse_bool(&g_config.unicode, item->str))
        cfgini_die(item, "Error: 'unicode' setting must be a boolean (on/off true/false 0/1)\n");
      state = INIT;
    } else if (state == KEYMAP && key < NKEYMAP) {
      if (parse_keymap(&g_config.keymap[key], item->str))
        cfgini_die(item, "Error: '%s' is not a valid keycode\n", item->str);
    }
    break;
  
  default: break;
  }
}

static int load_cfgfile()
{
  struct ifdstream stream;
  struct ini_parser parser;
  struct parser_receiver const receiver = {
    .receive = cfgini_receive
  };

  if (!g_config.cfgpath)
    return -1;

  int fd;

  fd = open(g_config.cfgpath, O_RDONLY);
  if (fd < 0) {
    perror("failed to open config file");
    alrc_put(g_config.cfgpath);
    g_config.cfgpath = NULL;
    return 0;
  }

  int result;

  result = ifdstream_init(fd, &stream);
  if (result)
    return result;

  result = ini_parser_init(&stream.istream, &receiver, &g_mallocator.allocator, &parser);
  if (result)
    return result;

  result = parser_run(&parser.parser);

  close(fd);

  return result;
}

static bool cmparg(char const *s, char const **optval, char const *arg, size_t arglen)
{
  int cmp = strncmp(s, arg, arglen);
  if (cmp)
    return false;

  usize const slen = strlen(s);

#ifndef NDEBUG
  assert(slen >= arglen);
#endif

  char const delim = s[arglen];
  if (delim == '\0') {
    /* this option either doesn't have a value, or its value is in the next arg */
    if (optval)
      *optval = NULL;
    return true;
  }

  if (delim == '=' && optval) {
    if (slen < arglen + 2)
      die("missing option value after =: '%s'\n", s);

    *optval = &s[arglen+1];
    return true;
  }

  return false;
}

static bool parseopt_bool(char const *opt)
{
  bool result;

  if (parse_bool(&result, opt)) {
    die("Expected a boolean value: %s\n", opt);
  }

  return result;
}

static int parse_args(int argc, char **argv)
{
  enum {
    OK,
    PARSERR,
    COLORARG,
    UNICODEARG,
  } state = OK;

  bool cmp;
  char const *opt;

  for (int i = 1; i < argc && argv[i]; ++i) {
    char const *s = argv[i];

    if (state == OK) {
      opt = NULL;

      cmp = cmparg(s, &opt, "--colors", sizeof("--colors")-1);
      if (opt) { /* --colors=bool */
        g_config.colors = parseopt_bool(opt);
        continue;
      } else if (cmp) { /* --colors */
        state = COLORARG;
        continue;
      }

      cmp = cmparg(s, &opt, "--unicode", sizeof("--unicode")-1);
      if (opt) { /* --unicode=bool */
        g_config.unicode = parseopt_bool(opt);
        continue;
      } else if (cmp) {
        state = UNICODEARG;
        continue;
      }

      cmp = cmparg(s, NULL, "--version", sizeof("--version")-1);
      if (cmp) {
        g_config.version = true;
        continue;
      }

      cmp = cmparg(s, NULL, "--help", sizeof("--help")-1);
      if (cmp) {
        g_config.help = true;
        continue;
      }

      cmp = cmparg(s, NULL, "--init", sizeof("--init")-1);
      if (cmp) {
        g_config.init = true;
        continue;
      }

      die("unexpected command line arg: %s\n", s);

    } else if (state == COLORARG) {
      g_config.colors = parseopt_bool(s);
      state = OK;

    } else if (state == UNICODEARG) {
      g_config.unicode = parseopt_bool(s);
      state = OK;
    }
  }
  
  switch (state) {
  case COLORARG:
    die("missing option - was expecting a value for --colors");
    break;
  case UNICODEARG:
    die("missing option - was expecting a value for --unicode");
    break;
  default:
    break;
  }

  return 0;
}
