/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "ui.h"
#include <ncurses.h>

#define COLON 10
#define SPACE 11

struct bignum {
  usize height, width;
  char const *strs[7];
};

struct ubignum {
  usize height, width;
  wchar_t const *strs[7];
};

static struct bignum ascii[12] = {
  {5,7, { " 0000",
          "00  00",
          "00  00",
          "00  00",
          " 0000",
        }},
  {5,7, { "1111",
          "  11",
          "  11",
          "  11",
          "111111",
        }},
  {5,7, { " 2222",
          "22  22",
          "   22",
          "  22",
          "222222",
        }},
  {5,7, { " 3333",
          "33  33",
          "   333",
          "33  33",
          " 3333",
        }},
  {5,7, { "44  44",
          "44  44",
          "444444",
          "    44",
          "    44",
        }},
  {5,7, { "555555",
          "55",
          "55555",
          "    55",
          "55555",
        }},
  {5,7, { " 6666",
          "66",
          "66666",
          "66  66",
          " 6666",
        }},
  {5,7, { "777777",
          "   77",
          "  77",
          " 77",
          "77",
        }},
  {5,7, { " 8888",
          "88  88",
          " 8888",
          "88  88",
          " 8888",
        }},
  {5,7, { " 9999",
          "99  99",
          " 99999",
          "    99",
          " 9999",
        }},
  {5,3, { "",
          "| ",
          "",
          "| ",
          "",
        }},
  {5,1, { " ",
          " ",
          " ",
          " ",
          " ",
        }}
};

static struct ubignum unicode[12] = {
  {5,7, { L"\u2554\u2550\u2550\u2550\u2557",
          L"\u2551\u2572  \u2551",
          L"\u2551 \u2572 \u2551",
          L"\u2551  \u2572\u2551",
          L"\u255a\u2550\u2550\u2550\u255d",
        }},
  {5,7, { L"\u2550\u2550\u2557",
          L"  \u2551",
          L"  \u2551",
          L"  \u2551",
          L"\u2550\u2550\u2569\u2550\u2550",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2557",
          L"    \u2551",
          L"\u2554\u2550\u2550\u2550\u255d",
          L"\u2551",
          L"\u255a\u2550\u2550\u2550\u2550",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2557",
          L"    \u2551",
          L" \u2550\u2550\u2550\u2563",
          L"    \u2551",
          L"\u255a\u2550\u2550\u2550\u255d",
        }},
  {5,7, { L"\u2553   \u2553",
          L"\u2551   \u2551",
          L"\u255a\u2550\u2550\u2550\u2563",
          L"    \u2551",
          L"    \u2559",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2550",
          L"\u2551",
          L"\u255a\u2550\u2550\u2550\u2557",
          L"    \u2551",
          L"\u2550\u2550\u2550\u2550\u255d",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2555",
          L"\u2551",
          L"\u2560\u2550\u2550\u2550\u2557",
          L"\u2551   \u2551",
          L"\u255a\u2550\u2550\u2550\u255d",
        }},
  {5,7, { L"\u2552\u2550\u2550\u2550\u2557",
          L"    \u2551",
          L"    \u2551",
          L"    \u2551",
          L"    \u2559",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2557",
          L"\u2551   \u2551",
          L"\u2560\u2550\u2550\u2550\u2563",
          L"\u2551   \u2551",
          L"\u255a\u2550\u2550\u2550\u255d",
        }},
  {5,7, { L"\u2554\u2550\u2550\u2550\u2557",
          L"\u2551   \u2551",
          L"\u255a\u2550\u2550\u2550\u2563",
          L"    \u2551",
          L"\u255a\u2550\u2550\u2550\u255d",
        }},
  {5,3, { L"",
          L"| ",
          L"",
          L"| ",
          L"",
        }},
  {5,1, { L" ",
          L" ",
          L" ",
          L" ",
          L" ",
        }}
};

static void print_num(struct bignum const *num)
{
  int y, x;
  getyx(stdscr, y, x);

  for (usize i = 0; i < num->height; ++i) {
    addstr(num->strs[i]);
    move(++y, x);
  }
}

static void wprint_num(struct ubignum const *num)
{
  int y, x;
  getyx(stdscr, y, x);

  for (usize i = 0; i < num->height; ++i) {
    addwstr(num->strs[i]);
    move(++y, x);
  }
}


void print_time(int secs)
{
  int const s = secs % 60;
  int const m = (secs / 60) % 100;

  int y, x;
  getyx(stdscr, y, x);

  if (g_config.unicode) {
    wprint_num(&unicode[m / 10]);
    move(y, x += unicode[m / 10].width);
    wprint_num(&unicode[m % 10]);
    move(y, x += unicode[m % 10].width);
    wprint_num(&unicode[COLON]);
    move(y, x += unicode[COLON].width);
    wprint_num(&unicode[s / 10]);
    move(y, x += unicode[s / 10].width);
    wprint_num(&unicode[s % 10]);
    move(y, x += unicode[s % 10].width);
  } else {
    print_num(&ascii[m / 10]);
    move(y, x += ascii[m / 10].width);
    print_num(&ascii[m % 10]);
    move(y, x += ascii[m % 10].width);
    print_num(&ascii[COLON]);
    move(y, x += ascii[COLON].width);
    print_num(&ascii[s / 10]);
    move(y, x += ascii[s / 10].width);
    print_num(&ascii[s % 10]);
    move(y, x += ascii[s % 10].width);
  }
}
