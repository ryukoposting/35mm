/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "main.h"
#include "ui.h"
#include "container.h"
#include <ctype.h>
#include <string.h>

static int sel_tmr = 0;
static bool active = false;

static enum selobj {
  TSET,
  TSET_IN,
  TSET_IN2,
  TSET_IN3,
  TSET_NAME,
  TSET_NAME_IN,
} cur_sel;

void set_sel_tmr(int n)
{
  sel_tmr = max(min((int)g_timerlist.length - 1, n), 0);
}

int get_sel_tmr()
{
  sel_tmr = max(min((int)g_timerlist.length - 1, sel_tmr), 0);
  return sel_tmr;
}

void set_timeredit_active(bool nactive)
{
  if (!active)
    cur_sel = TSET;
  active = nactive;
}

bool get_timeredit_active()
{
  return active;
}

void show_timeredit()
{
  int col, row;
  getmaxyx(stdscr, row, col);

  struct timer *seltmrp = NULL;
  unsigned selt = 0;
  int timeri = 0;
  char namebuf[TIMER_NAME_MAXLEN+1] = {};

  llist_eachit(&g_timerlist, {
    struct timer *timerp = container_of(IT, struct timer, llnode);
    unsigned t = ((read_timer(timerp) + (TICKS_PER_SEC - 1)) / TICKS_PER_SEC);
    if (timeri == get_sel_tmr()) {
      seltmrp = timerp;
      selt = t;
    }
    timeri++;
  });

  for (int r = 1; r < row; ++r) {
    if (g_config.colors)
      attron(COLOR_PAIR(C_CYAN));

    if (g_config.unicode)
      mvaddwstr(r, col/2, L"\u2502");
    else
      mvaddch(r, col/2, '|');

    if (g_config.colors)
      attroff(COLOR_PAIR(C_CYAN));
  }

  int const timerow = max(2, row / 6);

  
  usize const namesz = seltmrp ? strnlen(seltmrp->name, TIMER_NAME_MAXLEN) : 0;

  int const tmrcol = ((col * 3) / 4) - 14;
  int const namecol = max(((col * 3) / 4) - (namesz / 2), col/2u+1u);
  unsigned name_rowofs = 7;
  usize name_ul = 0;

  if (seltmrp) {
    snprintf(namebuf, sizeof(namebuf), "%s", seltmrp->name);
    move(timerow, tmrcol);
    print_time(selt);

    for (usize n = 0; n < namesz;) {
      /* set nprint to the max allowable line width */
      usize const nprint = min3(col/2u-1u, sizeof(namebuf) - 1u, namesz - n);

      snprintf(namebuf, nprint+1, "%s", &seltmrp->name[n]);

      move(timerow + name_rowofs, namecol);
      printw("%s", namebuf);

      name_ul = max(name_ul, nprint);
      n += nprint;
      ++name_rowofs;
    }
  }

  if (active) {
    if (g_config.colors)
      attron(COLOR_PAIR(C_MAGENTA));

    if (cur_sel >= TSET && cur_sel <= TSET_IN3) {
      move(timerow + 5, tmrcol);
      if (g_config.unicode)
        addwstr(L"\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500");
      else
        addstr("=============================");
    }

    if (cur_sel >= TSET_NAME && cur_sel <= TSET_NAME_IN) {
      move(timerow + name_rowofs, namecol);
      for (usize i = 0; i < name_ul; ++i) {
        if (g_config.unicode)
          addwstr(L"\u2500");
        else
          addstr("=");
      }
    }

    if (g_config.colors)
      attroff(COLOR_PAIR(C_MAGENTA));
  }
}

int feed_timeredit(int x)
{
  if (!active)
    return 1;

  int timeri = 0;
  struct timer *timerp = NULL;
  llist_eachit(&g_timerlist, {
    if (timeri == sel_tmr)
      timerp = container_of(IT, struct timer, llnode);
    timeri++;
  });

  if (x == KEY_LEFT) {
    active = false;
    return 0;

  } else if (cur_sel >= TSET && cur_sel <= TSET_IN3 && x == KEY_DC) {
    timerp->state = TSTOPPED;
    timerp->preset = 0;

  } else if (cur_sel == TSET && isdigit(x)) {
    timerp->state = TSTOPPED;
    if (x != '0')
      cur_sel = TSET_IN;
    timerp->preset = TICKS_PER_SEC * (x - '0');
    return 0;

  } else if (cur_sel == TSET_IN && isdigit(x)) {
    cur_sel = TSET_IN2;
    timerp->preset *= 10;
    timerp->preset += TICKS_PER_SEC * (x - '0');
    return 0;

  } else if (cur_sel == TSET_IN2 && isdigit(x)) {
    cur_sel = TSET_IN3;
    usize d1 = (timerp->preset / TICKS_PER_SEC) / 10;
    usize d2 = (timerp->preset / TICKS_PER_SEC) % 10;
    timerp->preset = d1 * TICKS_PER_SEC * 60;
    timerp->preset += d2 * TICKS_PER_SEC * 10;
    timerp->preset += TICKS_PER_SEC * (x - '0');
    return 0;

  } else if (cur_sel == TSET_IN3 && isdigit(x)) {
    cur_sel = TSET;
    usize d0 = (timerp->preset / TICKS_PER_SEC) / 60;
    timerp->preset -= d0 * TICKS_PER_SEC * 60;
    usize d1 = (timerp->preset / TICKS_PER_SEC) / 10;
    usize d2 = (timerp->preset / TICKS_PER_SEC) % 10;
    timerp->preset = d0 * TICKS_PER_SEC * 600;
    timerp->preset += d1 * TICKS_PER_SEC * 60;
    timerp->preset += d2 * TICKS_PER_SEC * 10;
    timerp->preset += TICKS_PER_SEC * (x - '0');
    return 0;

  } else if (cur_sel >= TSET && cur_sel <= TSET_IN3 && x == KEY_DOWN) {
    cur_sel = TSET_NAME;
    return 0;

  } else if (cur_sel >= TSET_NAME && cur_sel <= TSET_NAME_IN && x == KEY_UP) {
    cur_sel = TSET;
    return 0;

  } else if (cur_sel == TSET_NAME && isalnum(x)) {
    memset(timerp->name, 0, TIMER_NAME_MAXLEN);
    timerp->name[0] = x;
    cur_sel = TSET_NAME_IN;
    return 0;

  } else if (cur_sel == TSET_NAME_IN && (x == ' ' || isprint(x) || x == KEY_BACKSPACE)) {
    usize namelen = strnlen(timerp->name, TIMER_NAME_MAXLEN);

    if (x == '\b' || x == KEY_BACKSPACE) {
      if (namelen > 0) {
        timerp->name[namelen-1] = '\0';
      }
    } else if (namelen < TIMER_NAME_MAXLEN) {
      timerp->name[namelen] = x;
    }
  }

  return 0;
}
