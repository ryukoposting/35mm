PROJECT_NAME:=35mm
VERSION:=0.2.0
DEBUG_BUILD_DIR:=.build_debug
RELEASE_BUILD_DIR:=.build_release
PACKAGE_BUILD_DIR:=.package
CC:=gcc
LIBCORE_DIR:=libcore
LIBCORE_COMPONENTS:=\
	stream\
	list\
	array\
	mem\
	parser\
	table\
	hash

LINKFLAGS+=-lncursesw
CFLAGS+=-D_XOPEN_SOURCE_EXTENDED
CFLAGS+=-std=gnu17
CFLAGS+=-Wall -Wextra
