/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stdbool.h>
#include <wchar.h>
#include "mem/alrc.h"
#include "mem/mallocator.h"
#include "table/hashtable.h"
#include "list/linked.h"

#define TICKS_PER_SEC 10u
#define TICK_LENGTH_MSEC (1000u / TICKS_PER_SEC)
#define TIMER_NAME_MAXLEN 64
#define MAX_N_TIMERS 12
#define DEFAULT_CFGFILE_PATH "~/.config/35mm"

enum keymap {
  KEXIT,
  KNEWTIMER,
  KSETTINGS,
  KDELTIMER,
  KSTARTTIMER,
  KRESETTIMER,
  KPAUSETIMER,
  KMOVEUP,
  KMOVEDOWN,

  NKEYMAP
};

struct config {
  wchar_t keymap[NKEYMAP];
  char *cfgpath;
  bool colors;
  bool unicode;
  bool version;
  bool help;
  bool init;
};

enum timer_state {
  TSTOPPED,
  TPAUSED,
  TRUNNING,
  TFINISHED,
};

struct timer {
  struct llnode llnode;
  char *name;
  unsigned preset;
  unsigned start;
  unsigned paused_at;
  enum timer_state state;
  unsigned marquee_state;
};

extern struct mallocator g_mallocator;
extern struct alrc g_alrc;
extern struct config g_config;
extern struct llist g_timerlist;

void die(char const *msg, ...) __attribute__((noreturn));


char *default_cfgpath();


int new_timer(char const *name, unsigned preset_sec);

int start_timer(struct timer *timer);

int stop_timer(struct timer *timer);

int pause_timer(struct timer *timer);

int delete_timer(struct timer *timer);

unsigned read_timer(struct timer const *timer);

int update_timer(struct timer *timer);

int move_timer(int distance, struct timer *timer);


void load_config(int argc, char **argv);
