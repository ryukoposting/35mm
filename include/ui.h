/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "macros.h"
#include "main.h"
#include "ints.h"
#include <wchar.h>
#include <ncurses.h>

#define cprintwf(row, col, str, ...) mvprintw(row, col - (sizeof(str) / 2), str, __VA_ARGS__)
#define cprintw(row, col, str) mvprintw(row, col - (sizeof(str) / 2), str)

enum ui_elems {
  UI_WELCOME,
  UI_TOOLBAR,
  UI_TIMEREDIT,
  UI_TIMERLIST,
};

enum ui_colorpairs {
  C_RED,
  C_CYAN,
  C_WHITE,
  C_MAGENTA,
};

void show();
int feed(int x);
wchar_t k2wc(int x);
bool kallowed(int x);
void tick();
void print_time(int secs);

unsigned get_ticks();

void ui_enable(enum ui_elems elem);
void ui_disable(enum ui_elems elem);
bool ui_is_enabled(enum ui_elems elem);

void show_toolbar();
void show_welcome();
void show_settings();
void show_timerlist();
void show_timeredit();

void hide_settings();

int feed_toolbar(int x);
int feed_welcome(int x);
int feed_settings(int x);
int feed_timerlist(int x);
int feed_timeredit(int x);

void set_sel_tmr(int n);
int get_sel_tmr();
void set_timeredit_active(bool nactive);
bool get_timeredit_active();
