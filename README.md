# 35mm - Tiny, Beautiful Development Timer in the Terminal

*35mm* is a simple countdown timer that runs from a terminal. Its intended purpose is at-home film development, but obviously it can be used for anything where a countdown timer is useful.

*35mm* allows you to create multiple timers, and control them independently. Keybindings can be configured through an optional settings file.

# Installation

## Debian/Ubuntu Linux

*35mm* and its documentation are packaged together in a convenient .deb file for AMD64. Check the git tags to find the latest release.

## Other GNU/Linuxes

`sudo make install` should do the trick on pretty much any GNU/Linux system. Make sure you have libncursesw and git installed. If you have trouble compiling or linking, try fiddling with config.mk.

## Windows

Using `sudo make install` in MSYS2 or WSL is your best bet. Make sure ncursesw and git are installed. I have not tried compiling *35mm* in MinGW with pdcurses (yet).

## MacOS

Try installing ncursesw through homebrew. If `sudo make install` still doesn't work, use GCC instead of Apple's idiotic symlink to clang. I do not have the time or the patience to maintain a package for MacOS.

# Licensing

*35mm* is licensed under the Mozilla Public License (MPL), version 2.0.
