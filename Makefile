include config.mk
include $(LIBCORE_DIR)/libcore.mk

C_SOURCES+=$(wildcard src/*.c)
CFLAGS+=-I./include
CFLAGS+=-DVERSION="\"$(VERSION)\""
CFLAGS+=-DGIT_HASH="\"$(shell git rev-parse HEAD)\""
ifeq ($(OS),Windows_NT)
CFLAGS+=-DGIT_DIRTY=0
else
CFLAGS+=-DGIT_DIRTY=$(shell git status --porcelain | grep -E '^[ AM]{2}' | wc -l)
endif

ifeq ($(OS),Windows_NT)
EXE:=.exe
else
EXE:=
endif

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

DEPFLAGS=-MT $@ -MMD -MP -MF $(BUILD_DIR)/$*.d

debug: CFLAGS+=-O0 -g
debug: BUILD_DIR=$(DEBUG_BUILD_DIR)
debug: C_OBJECTS+=$(C_SOURCES:%.c=$(DEBUG_BUILD_DIR)/%.o)
debug: $(C_SOURCES:%.c=$(DEBUG_BUILD_DIR)/%.o)
debug: $(PROJECT_NAME)-debug$(EXE)

release: CFLAGS+=-O2 -g -DNDEBUG
release: BUILD_DIR=$(RELEASE_BUILD_DIR)
release: C_OBJECTS+=$(C_SOURCES:%.c=$(RELEASE_BUILD_DIR)/%.o)
release: $(C_SOURCES:%.c=$(RELEASE_BUILD_DIR)/%.o)
release: $(PROJECT_NAME)$(EXE)

install: release
	install -m 0755 $(PROJECT_NAME)$(EXE) /usr/local/bin
	cp doc/man/35mm.1 /usr/share/man/man1/35mm.1
	cp doc/man/35mm.ini.5 /usr/share/man/man5/35mm.ini.5
	gzip /usr/share/man/man1/35mm.1
	gzip /usr/share/man/man5/35mm.ini.5

package-debian: PACKAGE_DIR=$(PACKAGE_BUILD_DIR)/$(PROJECT_NAME)_$(VERSION)_$(ARCH)
package-debian: release
	mkdir -p $(PACKAGE_DIR)/usr/local/bin
	mkdir -p $(PACKAGE_DIR)/usr/share/man/man1
	mkdir -p $(PACKAGE_DIR)/usr/share/man/man5
	cp $(PROJECT_NAME)$(EXE) $(PACKAGE_DIR)/usr/local/bin
	cp doc/man/35mm.1 $(PACKAGE_DIR)/usr/share/man/man1
	cp doc/man/35mm.ini.5 $(PACKAGE_DIR)/usr/share/man/man5
	gzip $(PACKAGE_DIR)/usr/share/man/man1/35mm.1
	gzip $(PACKAGE_DIR)/usr/share/man/man5/35mm.ini.5
	mkdir -p $(PACKAGE_DIR)/DEBIAN
	cp dist/debian/control $(PACKAGE_DIR)/DEBIAN
	cp dist/debian/copyright $(PACKAGE_DIR)/DEBIAN
	cp dist/debian/changelog $(PACKAGE_DIR)/DEBIAN
	sed -i 's/%VERSION%/$(VERSION)/g' $(PACKAGE_DIR)/DEBIAN/control
	sed -i 's/%ARCH%/$(ARCH)/g' $(PACKAGE_DIR)/DEBIAN/control
	mkdir -p $(PACKAGE_DIR)/debian
	touch $(PACKAGE_DIR)/debian/control
	cd $(PACKAGE_DIR) && dpkg-shlibdeps usr/local/bin/$(PROJECT_NAME)$(EXE)
	sed -E 's/shlibs:([a-zA-Z]+)=(.*)/\1: \2/g' $(PACKAGE_DIR)/debian/substvars >> $(PACKAGE_DIR)/DEBIAN/control
	rm -rf $(PACKAGE_DIR)/debian
	dpkg-deb --build --root-owner-group $(PACKAGE_DIR)

$(PROJECT_NAME)-debug$(EXE): $(C_SOURCES:%.c=$(DEBUG_BUILD_DIR)/%.o)
	$(CC) $(CFLAGS) $(C_OBJECTS) -o $@ $(LINKFLAGS)

$(PROJECT_NAME)$(EXE): $(C_SOURCES:%.c=$(RELEASE_BUILD_DIR)/%.o)
	$(CC) $(CFLAGS) $(C_OBJECTS) -o $@ $(LINKFLAGS)

ifeq ($(OS),Windows_NT)
clean:
	-powershell "[void](Remove-Item -Recurse -Force $(DEBUG_BUILD_DIR))"
	-powershell "[void](Remove-Item -Recurse -Force $(RELEASE_BUILD_DIR))"
	-powershell "[void](Remove-Item -Recurse -Force $(PACKAGE_BUILD_DIR))"
else
clean:
	-rm -rf $(DEBUG_BUILD_DIR)
	-rm -rf $(RELEASE_BUILD_DIR)
	-rm -rf $(PACKAGE_BUILD_DIR)
endif

$(DEBUG_BUILD_DIR)/%.o: $(DEBUG_BUILD_DIR)/%.d
$(DEBUG_BUILD_DIR)/%.o: %.c Makefile config.mk
	$(info compiling $<)
ifeq ($(OS),Windows_NT)
	-@powershell "[void](New-Item $(dir $@) -ItemType Directory -ea 0)"
else
	-@mkdir -p $(dir $@)
endif
	$(CC) $(DEPFLAGS) $(CFLAGS) -c $< -o $@

$(RELEASE_BUILD_DIR)/%.o: $(RELEASE_BUILD_DIR)/%.d
$(RELEASE_BUILD_DIR)/%.o: %.c Makefile config.mk
	$(info compiling $<)
ifeq ($(OS),Windows_NT)
	-@powershell "[void](New-Item $(dir $@) -ItemType Directory -ea 0)"
else
	-@mkdir -p $(dir $@)
endif
	$(CC) $(DEPFLAGS) $(CFLAGS) -c $< -o $@
