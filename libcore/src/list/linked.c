/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "list/linked.h"

#include <stddef.h>

int llist_init(struct llist *llist)
{
  if (!llist)
    return -1;

  llist->head = NULL;
  llist->tailp = NULL;
  llist->length = 0;

  return 0;
}

int llnode_init(struct llnode *node)
{
  if (!node)
    return -1;

  node->next = NULL;
  return 0;
}

int llist_push_front(struct llnode *node, struct llist *llist)
{
  if (!node || !llist || node->next != NULL)
    return -1;

  node->next = llist->head;
  llist->head = node;
  if (llist->length == 0) {
    llist->tailp = &node->next;
  }
  llist->length += 1;

  return 0;
}

int llist_push_back(struct llnode *node, struct llist *llist)
{
  if (!node || !llist || node->next != NULL)
    return -1;

  node->next = NULL;
  if (llist->length > 0)
    *llist->tailp = node;
  llist->tailp = &node->next;
  if (llist->length == 0) {
    llist->head = node;
  }
  llist->length += 1;

  return 0;
}

struct llnode *llist_pop_front(struct llist *llist)
{
  if (!llist || llist->length == 0 || !llist->head)
    return NULL;

  struct llnode *result = llist->head;
  llist->head = result->next;
  result->next = NULL;
  llist->length -= 1;
  if (llist->length == 0) {
    llist->tailp = NULL;
  }

  return result;
}

// void *llist_pop_back(struct llist *llist);

int llist_remove(struct llnode *node, struct llist *llist)
{
  if (!node || !llist || llist->length == 0)
    return -1;

  struct llnode *p = llist->head, **prevnextp = &llist->head;

  while (p && p != node) {
    prevnextp = &p->next;
    p = p->next;
  }

  if (!p)
    return -1;

  if (llist->tailp == &p->next)
    llist->tailp = prevnextp;

  *prevnextp = p->next;
  p->next = NULL;
  llist->length -= 1;

  return 0;
}

int llist_insert(struct llnode *node, usize pos, struct llist *llist)
{
  if (!node || !llist || pos > llist->length || node->next)
    return -1;
  else if (pos == 0)
    return llist_push_front(node, llist);
  else if (pos == llist->length)
    return llist_push_back(node, llist);

  usize i = 0;
  llist_eachit(llist, {
    struct llnode *n = (struct llnode *)IT;
    if (++i == pos) {
      node->next = n->next;
      n->next = node;
      llist->length += 1;
      return 0;
    }
  });

  return -1;
}
