/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "array/circbuf.h"

#include <string.h>

static int grow(struct circbuf *circbuf);

int circbuf_init_raw(
  usize elemsz,
  usize nelems,
  void *buf,
  struct allocator const *allocator,
  struct circbuf *circbuf)
{
  if (!circbuf || (!buf && !allocator) || (buf && nelems == 0) || elemsz == 0)
    return -1;

  if (!buf && nelems > 0) {
    buf = allocate(elemsz * nelems, allocator);
    if (!buf)
      return -1;
  }

  circbuf->elemsz = elemsz;
  circbuf->nelems = nelems;
  circbuf->allocator = allocator;
  circbuf->bufp = buf;
  circbuf->start = 0;
  circbuf->len = 0;

  return 0;
}

int circbuf_push_back(void const *obj, struct circbuf *circbuf)
{
  if (!obj || !circbuf)
    return -1;
  
  if (circbuf->len >= circbuf->nelems) {
    if (!circbuf->allocator)
      return -1;

    if (grow(circbuf))
      return -1;
  }

  usize end = (circbuf->start + circbuf->len) % circbuf->nelems;

  memcpy(
    &((uint8*)circbuf->bufp)[end * circbuf->elemsz],
    obj,
    circbuf->elemsz);

  ++circbuf->len;

  return 0;
}

int circbuf_push_front(void const *obj, struct circbuf *circbuf)
{
  if (!obj || !circbuf)
    return -1;

  if (circbuf->len >= circbuf->nelems) {
    if (!circbuf->allocator)
      return -1;

    if (grow(circbuf))
      return -1;
  }

  usize front = circbuf->start ? circbuf->start - 1 : circbuf->nelems - 1;

  memcpy(
    &((uint8*)circbuf->bufp)[front * circbuf->elemsz],
    obj,
    circbuf->elemsz);

  circbuf->start = front;
  ++circbuf->len;

  return 0;
}

int circbuf_pop_back(void *obj, struct circbuf *circbuf)
{
  if (!obj || !circbuf)
    return -1;

  /* TODO */

  return 0;
}

int circbuf_pop_front(void *obj, struct circbuf *circbuf)
{
  if (!obj || !circbuf)
    return -1;

  /* TODO */

  return 0;
}

static int grow(struct circbuf *circbuf)
{
  usize const new_nelems = circbuf->len * 2u;
  usize const newsize = new_nelems * circbuf->elemsz;
  void *newbuf = reallocate(circbuf->bufp, newsize, circbuf->allocator);
  if (!newbuf)
    return -1;

  if (circbuf->start + circbuf->len > circbuf->nelems) {
    usize const endmove = (circbuf->start + circbuf->len) % circbuf->nelems;
    usize const offset = circbuf->nelems;
    for (usize i = 0; i < endmove; ++i) {
      memcpy(
        &((uint8*)newbuf)[((i + offset) % new_nelems) * circbuf->elemsz],
        &((uint8*)newbuf)[i * circbuf->elemsz],
        circbuf->elemsz);
    }
  }

  circbuf->bufp = newbuf;
  circbuf->nelems = new_nelems;
  return 0;
}

