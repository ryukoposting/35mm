/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "stream/arrstream.h"
#include "container.h"
#include "macros.h"
#include <string.h>

static isize read_impl(void *buf, usize count, struct istream const *istream)
{
  if (!buf || !istream)
    return -1;

  struct aistream *ip = container_of(istream, struct aistream, istream);
  if (!ip->p)
    return -1;

  if (ip->ofsp >= ip->sz)
    return 0;

  usize result = min(count, ip->sz - ip->ofsp);
  memcpy(buf, &((int8*)ip->p)[ip->ofsp], result);
  ip->ofsp += result;
  return result;
}

static isize seek_impl(long off, enum whence whence, struct istream const *istream)
{
  if (!istream)
    return -1;

  struct aistream *ip = container_of(istream, struct aistream, istream);

  switch (whence) {
  case WSET:
    ip->ofsp = off;
    break;
  case WCUR:
    ip->ofsp += off;
    break;
  case WEND:
    ip->ofsp = ip->sz + off;
    break;
  }

  return 0;
}

int aistream_init(void const *bufp, usize length, struct aistream *stream)
{
  if (!stream || ((!bufp) != (length == 0)))
    return -1;

  stream->ofsp = 0;
  stream->p = bufp;
  stream->sz = length;
  stream->istream.read = read_impl;
  stream->istream.seek = seek_impl;

  return 0;
}
