/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "stream/stream.h"

isize istream_bread(void *buf, isize count, int retry, struct istream const *istream)
{
  if (!buf || !istream)
    return -1;

  isize len = 0, ret = 0;
  for (int ntry = 0; ret >= 0 && len < count && (ntry < retry || retry == -1); ++ntry) {
    len += ret = stream_read(buf, count - len, istream);
  }

  if (ret < 0)
    return ret;
  else
    return len;
}

isize ostream_bwrite(void const *buf, isize count, int retry, struct ostream const *ostream)
{
  if (!buf || !ostream)
    return -1;

  isize len = 0, ret = 0;
  for (int ntry = 0; ret >= 0 && len < count && (ntry < retry || retry == -1); ++ntry) {
    len += ret = stream_write(buf, count - len, ostream);
  }

  if (ret < 0)
    return ret;
  else
    return len;
}

isize iostream_bread(void *buf, isize count, int retry, struct iostream const *iostream)
{
  if (!buf || !iostream)
    return -1;

  isize len = 0, ret = 0;
  for (int ntry = 0; ret >= 0 && len < count && (ntry < retry || retry == -1); ++ntry) {
    len += ret = stream_read(buf, count - len, iostream);
  }

  if (ret < 0)
    return ret;
  else
    return len;
}

isize iostream_bwrite(void const *buf, isize count, int retry, struct ostream const *iostream)
{
  if (!buf || !iostream)
    return -1;

  isize len = 0, ret = 0;
  for (int ntry = 0; ret >= 0 && len < count && (ntry < retry || retry == -1); ++ntry) {
    len += ret = stream_write(buf, count - len, iostream);
  }

  if (ret < 0)
    return ret;
  else
    return len;
}
