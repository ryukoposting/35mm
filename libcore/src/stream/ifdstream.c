/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "stream/fdstream.h"

#include "container.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static isize read_impl(void *buf, usize count, struct istream const *istream)
{
  if (!buf || !istream)
    return -1;

  struct ifdstream *ifp = container_of(istream, struct ifdstream, istream);

  return read(ifp->fd, buf, count);
}

static isize seek_impl(long off, enum whence whence, struct istream const *istream)
{
  if (!istream)
    return -1;

  struct ifdstream *ifp = container_of(istream, struct ifdstream, istream);

  return lseek(ifp->fd, off, whence);
}

int ifdstream_init(int fd, struct ifdstream *stream)
{
  if (!stream || fd < 0)
    return -1;

  stream->fd = fd;
  stream->istream.read = read_impl;
  stream->istream.seek = seek_impl;

  return 0;
}
