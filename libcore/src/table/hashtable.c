/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "table/hashtable.h"
#include "container.h"
#include "hash/hash.h"

#define get_hashtable(tablep) container_of((tablep), struct hashtable, table);
#define get_htelem(llnodep) container_of((llnodep), struct ht_elem, llnode)

#define DATALEN(tablep) (1u << (tablep)->ldata)
#define HASHMASK(tablep) ((1u << (tablep)->ldata) - 1u)
#define DATASZ(tablep) (sizeof(struct llist) * DATALEN(tablep))

#define LDATA_MAX 24u

struct ht_elem {
  struct llnode llnode;
  void *value;
  uint64 hashval;
  /* TODO store key ptr to prevent bugs due to hash collisions.
    upside of NOT doing that = key lifetime can be less than table lifetime */
};

static void *get(void const *key, usize keysize, struct table const *table);
static void *remove_key(void const *key, usize keysize, struct table const *table);
static int put(void const *key, usize keysize, void *value, struct table const *table);

static int get_hash(void const *key, usize keysize, uint64 *result, struct hashtable_type const *type);
static int grow(struct hashtable *ht);
static int shrink(struct hashtable *ht);

int hashtable_type_init(
  struct allocator const *allocator,
  struct hash64 const *hash,
  struct hashtable_type *type)
{
  if (!allocator || !hash || !type)
    return -1;

  type->allocator = allocator;
  type->hash = hash;

  return 0;
}

int hashtable_init(
  struct hashtable_type const *type,
  struct hashtable *table)
{
  if (!type || !table)
    return -1;

  int ret;

  ret = table_init(get, put, remove_key, &table->table);
  if (ret < 0)
    return ret;

  table->nelems = 0;
  table->type = type;
  table->ldata = 3;
  table->data = allocate(DATASZ(table), table->type->allocator);
  if (!table->data)
    return -1;

  for (usize i = 0; ret == 0 && i < DATALEN(table); ++i) {
    ret = llist_init(&table->data[i]);
  }

  return ret;
}

static void *get(void const *key, usize keysize, struct table const *table)
{
  struct hashtable *ht = get_hashtable(table);
  struct hashtable_type const *type = ht->type;

  uint64 const mask = HASHMASK(ht);
  if (mask == 0 || !ht->data)
    return NULL;

  int ret;
  uint64 hashval;

  ret = get_hash(key, keysize, &hashval, type);
  if (ret < 0)
    return NULL;

  uint64 const listi = hashval & mask;

  struct llnode *node = llist_findit(&ht->data[listi], 
    struct ht_elem *ep = get_htelem(IT);
    ep->hashval == hashval;
  );

  if (!node)
    return NULL;

  struct ht_elem *ep = get_htelem(node);

  return ep->value;
}

static void *remove_key(void const *key, usize keysize, struct table const *table)
{
  struct hashtable *ht = get_hashtable(table);
  struct hashtable_type const *type = ht->type;

  uint64 const mask = HASHMASK(ht);
  if (mask == 0 || !ht->data || ht->nelems == 0)
    return NULL;

  int ret;
  uint64 hashval;

  ret = get_hash(key, keysize, &hashval, type);
  if (ret < 0)
    return NULL;

  uint64 const listi = hashval & mask;
  struct llist *const listp = &ht->data[listi];

  struct llnode *node = llist_findit(listp, 
    struct ht_elem *ep = get_htelem(IT);
    ep->hashval == hashval;
  );

  if (!node)
    return NULL;

  ret = llist_remove(node, listp);
  if (ret < 0)
    return NULL;

  struct ht_elem *ep = get_htelem(node);
  void *result = ep->value;

  release(ep, type->allocator);
  ht->nelems -= 1;

  if (ht->ldata > 3) {
    usize const threshold = (1 << (ht->ldata-1)) - (DATALEN(ht) / ht->ldata);
    if (ht->nelems < threshold)
      shrink(ht);
  }

  return result;
}

static int put(void const *key, usize keysize, void *value, struct table const *table)
{
  struct hashtable *ht = get_hashtable(table);
  struct hashtable_type const *type = ht->type;

  uint64 const mask = HASHMASK(ht);
  if (mask == 0 || !ht->data)
    return -1;

  int ret;
  uint64 hashval;

  ret = get_hash(key, keysize, &hashval, type);
  if (ret < 0)
    return ret;

  uint64 const listi = hashval & mask;
  struct llist *const listp = &ht->data[listi];

  struct llnode *node = llist_findit(listp, 
    struct ht_elem *ep = get_htelem(IT);
    ep->hashval == hashval;
  );

  if (node) {
    struct ht_elem *ep = get_htelem(node);
    ep->value = value;
    return 0;
  }

  struct ht_elem *newelem = allocate(sizeof(*newelem), type->allocator);
  if (!newelem)
    return -1;

  ret = llnode_init(&newelem->llnode);
  if (ret < 0)
    return ret;

  newelem->hashval = hashval;
  newelem->value = value;

  ret = llist_push_front(&newelem->llnode, listp);
  if (ret < 0)
    return ret;

  ht->nelems += 1;

  if (ht->ldata < LDATA_MAX) {
    usize const threshold = DATALEN(ht) - (DATALEN(ht) / ht->ldata);
    if (ht->nelems > threshold)
      grow(ht);
  }

  return 0;
}

static int get_hash(void const *key, usize keysize, uint64 *result, struct hashtable_type const *type)
{
  int ret;

  ret = hash64_start(result, type->hash);
  if (ret < 0)
    return ret;

  return hash64_digest(key, keysize, result, type->hash);
}

static int replace_all(struct hashtable *ht, usize oldlen)
{
  uint64 const mask = HASHMASK(ht);

  int ret;
  struct llist temp_llist;

  ret = llist_init(&temp_llist);
  if (ret < 0)
    return ret;

  for (usize i = 0; i < oldlen; ++i) {
    struct llist *listp = &ht->data[i];

    /* move every element into the temporary list */
    /* TODO optimize this */
    while (listp->length > 0) {
      struct llnode *nodep = llist_pop_front(listp);
      llist_push_front(nodep, &temp_llist);
    }
  }

  /* put all the elements into the correct list */
  while (temp_llist.length > 0) {
    struct llnode *nodep = llist_pop_front(&temp_llist);
    struct ht_elem *elemp = get_htelem(nodep);
    uint64 const new_i = elemp->hashval & mask;
    llist_push_front(nodep, &ht->data[new_i]);
  }

  return ret;
}

static int grow(struct hashtable *ht)
{
  struct hashtable_type const *type = ht->type;
  if (ht->ldata >= (sizeof(usize) * 8) - 1)
    return 0;

  ht->ldata += 1;
  ht->data = reallocate(ht->data, DATASZ(ht), type->allocator);
  if (!ht->data)
    return -1;

  int ret = 0;

  for (usize i = DATALEN(ht) / 2; ret >= 0 && i < DATALEN(ht); ++i) {
    ret = llist_init(&ht->data[i]);
  }

  if (ret < 0)
    return ret;

  return replace_all(ht, DATALEN(ht) / 2);
}

static int shrink(struct hashtable *ht)
{
  struct hashtable_type const *type = ht->type;
  if (ht->ldata <= 0)
    return 0;

  int ret;

  ht->ldata -= 1;

  /* move all elements into the lower half of the table */
  ret = replace_all(ht, DATALEN(ht) * 2);
  if (ret < 0)
    return ret;

  ht->data = reallocate(ht->data, DATASZ(ht), type->allocator);
  if (!ht->data)
    return -1;

  return 0;
}
