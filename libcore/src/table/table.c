/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "table/table.h"
#include <stddef.h>

int table_init(
  void *(*get)(void const *key, usize keysize, struct table const *table),
  int (*put)(void const *key, usize keysize, void *value, struct table const *table),
  void *(*remove_key)(void const *key, usize keysize, struct table const *table),
  struct table *table)
{
  if (!table || !get || !put || !remove_key)
    return -1;

  table->get = get;
  table->put = put;
  table->remove_key = remove_key;

  return 0;
}

int table_delete(struct table *table)
{
  // TODO
  return 0;
}

void *table_get(void const *key, usize keysize, struct table const *table)
{
  if (!table || !table->get || (!key && keysize > 0))
    return NULL;

  return table->get(key, keysize, table);
}

int table_put(void const *key, usize keysize, void *value, struct table const *table)
{
  if (!table || !table->put || (!key && keysize > 0))
    return -1;

  return table->put(key, keysize, value, table);
}

void *table_remove(void const *key, usize keysize, struct table const *table)
{
  if (!table || !table->remove_key || (!key && keysize > 0))
    return NULL;

  return table->remove_key(key, keysize, table);
}
