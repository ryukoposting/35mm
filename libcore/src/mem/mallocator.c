/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "mem/mallocator.h"

#include "macros.h"
#include <stdlib.h>

static void *m_alloc(usize nbytes, struct allocator const *allocator)
{
  unused(allocator);
  return malloc(nbytes);
}

static void *m_realloc(void *p, usize nbytes, struct allocator const *allocator)
{
  unused(allocator);
  return realloc(p, nbytes);
}

static void m_free(void *p, struct allocator const *allocator)
{
  unused(allocator);
  free(p);
}

int mallocator_init(struct mallocator *mallocator)
{
  if (!mallocator)
    return -1;

  mallocator->allocator.alloc = m_alloc;
  mallocator->allocator.realloc = m_realloc;
  mallocator->allocator.free = m_free;

  return 0;
}
