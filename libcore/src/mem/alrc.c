/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "mem/alrc.h"
#include "container.h"
#include "macros.h"

struct alrc_obj {
  struct rc rc;
  void (*on_release)(void*);
};

static void get(struct rc const *rc, isize count);
static void put(struct rc const *rc, isize count);

int alrc_init(struct allocator const *allocator, struct alrc *rctype)
{
  if (!allocator || !rctype)
    return -1;

  rctype->allocator = allocator;
  return rctype_init(get, put, &rctype->rctype);
}

void *alrc_alloc(usize nbytes, void (*on_release)(void*), struct alrc *alrc)
{
  if (!alrc || nbytes == 0)
    return NULL;

  void *ap = allocate(sizeof(struct alrc_obj) + nbytes, alrc->allocator);
  struct alrc_obj *obj = (struct alrc_obj*)ap;

  int ret = rc_init(&alrc->rctype, &obj->rc);
  if (ret < 0)
    return NULL;

  obj->on_release = on_release;

  return ((int8*)ap) + sizeof(struct alrc_obj);
}

int alrc_get(void *p)
{
  if (!p)
    return -1;

  void *ap = ((int8*)p) - sizeof(struct alrc_obj);
  struct alrc_obj *obj = (struct alrc_obj*)ap;

  return rc_get(&obj->rc);
}

int alrc_put(void *p)
{
  if (!p)
    return -1;

  void *ap = ((int8*)p) - sizeof(struct alrc_obj);
  struct alrc_obj *obj = (struct alrc_obj*)ap;

  return rc_put(&obj->rc);
}

static void get(struct rc const *rc, isize count)
{
  unused(rc);
  unused(count);
}

static void put(struct rc const *rc, isize count)
{
  unused(rc);

  struct alrc_obj *objp = container_of(rc, struct alrc_obj, rc);
  struct alrc *alrc = container_of(rc->rctype, struct alrc, rctype);

  if (count == 0) {
    if (objp->on_release)
      objp->on_release(((int8*)objp) + sizeof(struct alrc_obj));
    release(objp, alrc->allocator);
  }
}
