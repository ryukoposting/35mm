/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "mem/rc.h"

int rctype_init(void (*get)(struct rc const *rc, isize count), void (*put)(struct rc const *rc, isize count), struct rctype *rctype)
{
  if (!get || !put || !rctype)
    return -1;

  rctype->get = get;
  rctype->put = put;

  return 0;
}

int rc_init(struct rctype const *rctype, struct rc *rc)
{
  if (!rctype || !rc)
    return -1;

  rc->rctype = rctype;
  rc->count = 1;

  return 0;
}

int rc_get(struct rc *rc)
{
  if (!rc || !rc->rctype || !rc->rctype->get || rc->count <= 0)
    return -1;
  else {
    rc->count += 1;
    rc->rctype->get(rc, rc->count);
    return 0;
  }
}

int rc_put(struct rc *rc)
{
  if (!rc || !rc->rctype || !rc->rctype->put)
    return -1;
  else {
    rc->count -= 1;
    rc->rctype->put(rc, rc->count);
    return 0;
  }
}
