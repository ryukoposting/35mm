/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "parself/reader.h"

#include "stream/stream.h"

#define decode_u16(hdrp, ptr) (elf_hdr_is_le(hdrp) ? decode_u16_le(ptr) : decode_u16_be(ptr))
#define decode_u32(hdrp, ptr) (elf_hdr_is_le(hdrp) ? decode_u32_le(ptr) : decode_u32_be(ptr))
#define decode_u64(hdrp, ptr) (elf_hdr_is_le(hdrp) ? decode_u64_le(ptr) : decode_u64_be(ptr))

static int read_header(struct elf_reader *reader);
static isize push(struct elf_reader *reader);
static isize pop(struct elf_reader *reader);

int elf_read_start(struct istream *istream, struct elf_reader *reader)
{
  if (!istream || !reader)
    return -1;

  reader->istream = istream;
  reader->offsp = 0;
  reader->err_at = 0;
  reader->err_cause = 0;

  return read_header(reader);
}

int elf_read_push(struct elf_reader *reader)
{
  if (!reader || reader->offsp >= ELF_READER_MAXPUSH - 1)
    return -1;

  ++reader->offsp;

  return 0;
}

int elf_read_pop(struct elf_reader *reader)
{
  if (!reader || reader->offsp <= 0)
    return -1;

  --reader->offsp;

  return 0;
}

int elf_read_phdr(struct elf_phdr *phdr, int entno, struct elf_reader *reader)
{

}

int elf_next_phdr(struct elf_phdr *phdr, struct elf_reader *reader)
{

}

int elf_read_shdr(struct elf_phdr *phdr, int entno, struct elf_reader *reader)
{

}

int elf_next_shdr(struct elf_phdr *phdr, struct elf_reader *reader)
{

}

static int read_header(struct elf_reader *reader)
{
  isize ofs, ret;
  uint8 rbuf[48];

  struct elf_hdr *hdr = &reader->hdr;

  ofs = ret = stream_seek(0, WSET, reader->istream);
  if (ret < 0)
    return -1;

  ofs += ret = stream_bread(hdr->base, ELF_HDR_BASE_LEN, -1, reader->istream);
  if (ret < 0)
    return -1;

  /* TODO check header */

  usize ntoread;
  if (elf_hdr_is_elf32(hdr))
    ntoread = 36;
  else if (elf_hdr_is_elf64(hdr))
    ntoread = 48;
  else
    return -1;

  ofs = ret = stream_seek(7, WCUR, reader->istream);
  if (ret < 0)
    return -1;

  ofs += ret = stream_bread(rbuf, ntoread, -1, reader->istream);
  if (ret < 0 || ret < ntoread)
    return -1;

  usize pos = 0;
  hdr->e_type = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_type);

  hdr->e_machine = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_machine);

  hdr->e_version = decode_u32(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_version);

  if (elf_hdr_is_elf32(hdr)) {
    hdr->e_entry = decode_u32(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_entry);
  } else {
    hdr->e_entry_64 = decode_u64(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_entry_64);
  }

  if (elf_hdr_is_elf32(hdr)) {
    hdr->e_phoff = decode_u32(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_phoff);
  } else {
    hdr->e_phoff_64 = decode_u64(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_phoff_64);
  }

  if (elf_hdr_is_elf32(hdr)) {
    hdr->e_shoff = decode_u32(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_shoff);
  } else {
    hdr->e_shoff_64 = decode_u64(hdr, &rbuf[pos]);
    pos += sizeof(hdr->e_shoff_64);
  }

  hdr->e_flags = decode_u32(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_flags);

  hdr->e_ehsize = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_ehsize);

  hdr->e_phentsize = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_phentsize);

  hdr->e_phnum = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_phnum);

  hdr->e_shentsize = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_shentsize);

  hdr->e_shnum = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_shnum);

  hdr->e_shstrndx = decode_u16(hdr, &rbuf[pos]);
  pos += sizeof(hdr->e_shstrndx);

  ofs = ret = stream_seek(2, WCUR, reader->istream);
  if (ret < 0)
    return -1;

  reader->offs[reader->offsp] = ofs;
  return 0;
}

static isize push(struct elf_reader *reader)
{

}

static isize pop(struct elf_reader *reader)
{

}
