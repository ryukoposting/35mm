/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "parser/parser.h"

int parser_run(struct parser const *parser)
{
  if (!parser || !parser->run)
    return -1;

  return parser->run(parser);
}

int parser_pause(struct parser const *parser)
{
  if (!parser || !parser->pause)
    return -1;

  return parser->pause(parser);
}
