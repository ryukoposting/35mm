/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "parser/ini.h"
#include "container.h"
#include "macros.h"
#include <regex.h>
#include <string.h>

#define get_ini_parser(parsep) container_of(parsep, struct ini_parser, parser)

enum state {
  INITIAL,
  BUFFERING,
  PARSING,
  BUFFERING_INOBJECT,
  PARSING_INOBJECT,
};

static int pause(struct parser const *parser);
static int run(struct parser const *parser);
static int resize_buf(struct ini_parser *ini, usize newsize);

int ini_parser_init(
  struct istream const *stream,
  struct parser_receiver const *receiver,
  struct allocator const *allocator,
  struct ini_parser *parser)
{
  if (!stream || !receiver || !allocator || !parser)
    return -1;

  parser->parser.pause = pause;
  parser->parser.run = run;
  parser->parser.receiver = receiver;
  parser->parser.stream = stream;
  parser->allocator = allocator;
  parser->tmpbuf = NULL;
  parser->tmpbuf_sz = 0;
  parser->max_line_len = 512;
  parser->run = false;
  parser->nbytes = 0;
  parser->state = INITIAL;

  return 0;
}

static int pause(struct parser const *parser)
{
  struct ini_parser *const ini = get_ini_parser(parser);
  ini->run = false;
  return 0;
}

static int run(struct parser const *parser)
{
  struct ini_parser *const ini = get_ini_parser(parser);
  struct parser_receiver const *const receiver = parser->receiver;

  ini->run = true;

  struct parse_item item = {};

  usize starti = 0, endi = 0;
  isize nread = 0;

  int ret = 0;
  regex_t obj_re, kv_re, comment_re;

  ret = regcomp(&obj_re, "^\\[[[:alnum:] :_-]+\\]", REG_NEWLINE | REG_EXTENDED);
  if (ret)
    return ret;

  ret = regcomp(&kv_re, "^[[:alnum:] :_-]+=[^;#]+", REG_NEWLINE | REG_EXTENDED);
  if (ret) {
    regfree(&obj_re);
    return ret;
  }

  ret = regcomp(&comment_re, "^[;#].*", REG_NEWLINE | REG_EXTENDED);
  if (ret) {
    regfree(&kv_re);
    regfree(&obj_re);
    return ret;
  }

  while (ini->run && ret == 0) {
    if (ini->state == INITIAL) {
      item.lineinfo.line = 1;
      item.lineinfo.column = 0;
      item.type = PARSE_OBJ_START;
      item.str = NULL;
      item.msg = NULL;
      receiver->receive(&item, parser);
      ini->nbytes = 0;
      ini->state = BUFFERING;

    } else if (ini->state == BUFFERING || ini->state == BUFFERING_INOBJECT) {
      if (ini->tmpbuf_sz == 0 || !ini->tmpbuf) {
        ini->nbytes = 0;
        ret = resize_buf(ini, 64);
      } else if (ini->nbytes + 1 >= ini->tmpbuf_sz) {
        ret = resize_buf(ini, ini->nbytes * 2);
      } else if (ini->nbytes + 1 >= ini->max_line_len + 1) {
        ret = -1;
      }

      if (ret < 0)
        break;

      nread = stream_read(&ini->tmpbuf[ini->nbytes], ini->tmpbuf_sz - 1 - ini->nbytes, parser->stream);
      if (nread < 0) {
        item.type = PARSE_ERROR;
        item.str = NULL;
        item.msg = "stream error";
        receiver->receive(&item, parser);
        ret = -1;
        break;
      }

      ini->nbytes += nread;
      ini->tmpbuf[ini->nbytes] = '\0';

      if (ini->nbytes == 0)
        break;
      else if (ini->state == BUFFERING)
        ini->state = PARSING;
      else
        ini->state = PARSING_INOBJECT;

    } else if (ini->state == PARSING || ini->state == PARSING_INOBJECT) {
      starti = endi = 0;
      /* set start index */
      while (starti < ini->nbytes &&
             (ini->tmpbuf[starti] == '\r' || ini->tmpbuf[starti] == '\n')) {
        if (ini->tmpbuf[starti] == '\n') {
          item.lineinfo.line += 1;
          item.lineinfo.column = 0;
        }
        starti += 1;
        endi += 1;
      }

      /* find end index (if possible) */
      while (endi < ini->nbytes &&
             (ini->tmpbuf[endi] != '\r' && ini->tmpbuf[endi] != '\n')) {
        endi += 1;
      }

      /* didn't find an end - shift bytes back and buffer more bytes */
      /* ini->nbytes `+ 1` moves the null terminator forward */
      if (endi >= ini->nbytes) {
        if (nread == 0) {
          /* reached end of buffer. */
          item.type = PARSE_OBJ_END;
          receiver->receive(&item, parser);
          if (ini->state == PARSING_INOBJECT) {
            receiver->receive(&item, parser);
          }
          break; 
        }
        for (usize i = starti; i < ini->nbytes + 1; ++i) {
          ini->tmpbuf[i - starti] = ini->tmpbuf[i];
        }
        ini->nbytes -= starti;
        if (ini->state == PARSING)
          ini->state = BUFFERING;
        else
          ini->state = BUFFERING_INOBJECT;
        continue;
      }

      char *linep = &ini->tmpbuf[starti];
      regmatch_t mat;

      /* figure out what kind of line this is */
      if (0 == regexec(&comment_re, linep, 1, &mat, 0) && mat.rm_so == 0) {
        linep[mat.rm_eo] = '\0';
        item.lineinfo.line += 1;
      } else if (0 == regexec(&obj_re, linep, 1, &mat, 0) && mat.rm_so == 0) {
        linep[mat.rm_eo] = '\0';
        linep[mat.rm_eo-1] = '\0';
        linep[mat.rm_so] = '\0';

        item.lineinfo.column = 0;
        item.str = linep + 1;

        if (ini->state == PARSING_INOBJECT) {
          item.type = PARSE_OBJ_END;
          receiver->receive(&item, parser);
        }

        item.type = PARSE_KEY;
        receiver->receive(&item, parser);

        item.type = PARSE_OBJ_START;
        receiver->receive(&item, parser);

        item.lineinfo.line += 1;
        ini->state = PARSING_INOBJECT;

      } else if (0 == regexec(&kv_re, linep, 1, &mat, 0) && mat.rm_so == 0) {
        linep[mat.rm_eo] = '\0';
        char *key = linep;
        char *value = strstr(linep, "=");
        *value = '\0';
        ++value;

        item.lineinfo.column = 0;
        item.str = key;
        item.type = PARSE_KEY;
        receiver->receive(&item, parser);

        item.lineinfo.column = value - key;
        item.str = value;
        item.type = PARSE_STR;
        receiver->receive(&item, parser);

        item.lineinfo.line += 1;
      } else {
        linep[endi] = '\0';
        item.type = PARSE_ERROR;
        item.str = linep;
        item.msg = "syntax error";
        receiver->receive(&item, parser);
        ret = -1;
        break;
      }

      /* shift buffer forwards */
      /* ini->nbytes `+ 1` moves the null terminator forward */
      for (usize i = endi + 1; i < ini->nbytes + 1; ++i) {
        ini->tmpbuf[i - (endi + 1)] = ini->tmpbuf[i];
      }
      ini->nbytes -= (endi + 1);
    }
  }

  regfree(&kv_re);
  regfree(&obj_re);
  regfree(&comment_re);

  return ret;
}

static int resize_buf(struct ini_parser *ini, usize newsize)
{
  struct allocator const *const allocator = ini->allocator;

  /* restrict newsize to be <= max_line_len */
  if (newsize > ini->max_line_len)
    newsize = ini->max_line_len + 1;

  ini->tmpbuf = reallocate(ini->tmpbuf, newsize, allocator);
  if (!ini->tmpbuf) {
    ini->tmpbuf_sz = 0;
    if (newsize > 0)
      return -1;
  }

  ini->tmpbuf_sz = newsize;
  return 0;
}
