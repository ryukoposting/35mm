/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "hash/fnv.h"
#include "macros.h"

#define FNV1A_32_OFFSET_BASIS ((uint32)0x811c9dc5u)
#define FNV1A_32_PRIME ((uint32)0x01000193)
#define FNV1A_64_OFFSET_BASIS ((uint64)0xcbf29ce484222325u)
#define FNV1A_64_PRIME ((uint64)0x00000100000001B3u)


static int fnv1a_32_hinit(void *result, struct hash const *hash);
static int fnv1a_32_hdigest(void const *in, usize nbytes, void *result, struct hash const *hash);
static int fnv1a_64_hinit(void *result, struct hash const *hash);
static int fnv1a_64_hdigest(void const *in, usize nbytes, void *result, struct hash const *hash);


int fnv1a_32_init(struct hash32 *hash32)
{
  if (!hash32)
    return -1;

  return hash_init(4, fnv1a_32_hinit, fnv1a_32_hdigest, &hash32->hash);
}

int fnv1a_64_init(struct hash64 *hash64)
{
  if (!hash64)
    return -1;

  return hash_init(8, fnv1a_64_hinit, fnv1a_64_hdigest, &hash64->hash);
}

static int fnv1a_32_hinit(void *result, struct hash const *hash)
{
  unused(hash);

  if (!result)
    return -1;

  uint64 *result32 = (uint64*)result;
  *result32 = FNV1A_32_OFFSET_BASIS;
  return 0;
}

static int fnv1a_32_hdigest(void const *in, usize nbytes, void *result, struct hash const *hash)
{
  unused(hash);

  if (!result || (!in && nbytes > 0))
    return -1;

  uint32 *result32 = (uint32*)result;

  uint8 const *bytes = (uint8 const*)in;
  for (uint32 i = 0; i < nbytes; ++i) {
    *result32 ^= bytes[i];
    *result32 *= FNV1A_32_PRIME;
  }

  return 0;
}

static int fnv1a_64_hinit(void *result, struct hash const *hash)
{
  unused(hash);
  if (!result)
    return -1;
  uint64 *result64 = (uint64*)result;
  *result64 = FNV1A_64_OFFSET_BASIS;
  return 0;
}

static int fnv1a_64_hdigest(void const *in, usize nbytes, void *result, struct hash const *hash)
{
  unused(hash);
  if (!result || (!in && nbytes > 0))
    return -1;

  uint64 *result64 = (uint64*)result;

  uint8 const *bytes = (uint8 const*)in;
  for (uint32 i = 0; i < nbytes; ++i) {
    *result64 ^= bytes[i];
    *result64 *= FNV1A_64_PRIME;
  }

  return 0;
}
