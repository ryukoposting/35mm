/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "hash/hash.h"

int hash_init(
  usize buf_size,
  int (*init)(void *result, struct hash const *hash),
  int (*digest)(void const *in, usize nbytes, void *result, struct hash const *hash),
  struct hash *hash)
{
  if (!hash || !init || !digest || buf_size == 0)
    return -1;

  hash->buf_size = buf_size;
  hash->init = init;
  hash->digest = digest;

  return 0;
}

int hash_start(void *result, usize result_len, struct hash const *hash)
{
  if (!result || !hash || !hash->init || result_len != hash->buf_size)
    return -1;

  return hash->init(result, hash);
}

int hash_digest(void const *in, usize nbytes, void *result, usize result_len, struct hash const *hash)
{
  if (!result || !hash || !hash->digest || result_len != hash->buf_size)
    return -1;

  return hash->digest(in, nbytes, result, hash);
}

int hash32_start(uint32 *result, struct hash32 const *hash)
{
  if (!result || !hash || !hash->hash.init)
    return -1;

  return hash_start(result, sizeof(*result), &hash->hash);
}

int hash32_digest(void const *in, usize nbytes, uint32 *result, struct hash32 const *hash)
{
  if (!result || !hash || !hash->hash.digest || (!in && nbytes > 0))
    return -1;
  else if (nbytes == 0)
    return 0;

  return hash_digest(in, nbytes, result, sizeof(*result), &hash->hash);
}

int hash64_start(uint64 *result, struct hash64 const *hash)
{
  if (!result || !hash || !hash->hash.init)
    return -1;

  return hash_start(result, sizeof(*result), &hash->hash);
}

int hash64_digest(void const *in, usize nbytes, uint64 *result, struct hash64 const *hash)
{
  if (!result || !hash || !hash->hash.digest || (!in && nbytes > 0))
    return -1;
  else if (nbytes == 0)
    return 0;

  return hash_digest(in, nbytes, result, sizeof(*result), &hash->hash);
}
