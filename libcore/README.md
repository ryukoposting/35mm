# libcore

`libcore` is a work-in-progress library of modular C algorithms, data structures, and macros.

## Principles

`libcore` is built upon five core principles:

1. Modularity.
2. Orthogonality.
3. Compatibility.
4. Modernity.
5. Freedom.

### Modularity

`libcore` is made up of small, simple parts that can be reused over and over.

### Orthogonality

Each `libcore` module provides one meaningful piece of functionality that is not available in the ISO or POSIX `libc` standards.

### Compatibility

Except when absolutely (and *provably*) necessary, `libcore` makes no assumptions about any aspect of the dependent program's design or intended use case. `libcore`'s design makes absolutely no assumptions about the dependent program's...
  - Memory allocation strategy.
  - Hosting operating system, or...
  - ...whether there's an operating system at all.
  - Target instruction set.
  - Target architecture's word size.
  - Acceptable memory capacity.
  - Availability of an FPU.
  - Anything else, besides the availability of a modern C compiler.

### Modernity

C is an old language, but that doesn't mean we have to code like it's 1999. `libcore` uses C17 with language extensions that are readily available in any actively-maintained C compiler. `libcore` encourages the use of modern C constructs where appropriate, including (but not limited to):

  - [Designated initializers](https://gcc.gnu.org/onlinedocs/gcc/Designated-Inits.html)
  - [Statement Expressions](https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html)
  - `offsetof`
  - `typeof`
  - `_Generic`
  - etc.

### Freedom

`libcore` adheres to the [Four Essential Freedoms](https://www.gnu.org/philosophy/free-sw.html#four-freedoms), as defined by the Free Software Foundation.

`libcore` is free and open source. It may be used in any software, whether or not that software adheres to `libcore`'s perspective on software ethics.

`libcore` and its developers believe that good software makes a good-faith effort to be accessible to anyone who may find value in using it.


## Features (and Planned Features)

| Header                | Description                                  | Phase                      |
|:--------------------- |:-------------------------------------------- |:--------------------------- |
| `array/circbuf.h`     | Generic circular buffer.                     | Conceptual |
| `hash/hash.h`         | Generic hash function interface.             | Prototype |
| `hash/fnv.h`          | FNV hash function implementation.            | Prototype |
| `list/linked.h`       | Singly-linked list.                          | Development |
| `list/double.h`       | Doubly-linked list.                          | Conceptual |
| `list/circular.h`     | Circular singly-linked list.                 | Conceptual |
| `list/dcircular.h`    | Circular doubly-linked list.                 | Conceptual |
| `mem/allocator.h`     | Generic memory allocator interface.          | Development |
| `mem/mallocator.h`    | Bindings from stdlib functions to `mem/allocator.h` | Development |
| `mem/rc.h`            | Generic reference counter.                   | Development |
| `mem/alrc.h`          | Reference-counting allocator.                | Development |
| `mem/mark.h`          | Generic mark-and-sweep algorithm.            | Pre-conceptual |
| `mem/almark.h`        | Reference-counting mark-and-sweep garbage collector. | Pre-conceptual |
| `parself/reader.h`    | ELF file parser.                             | Prototype |
| `parser/parser.h`     | Generic interface for parsers of structured data. | Prototype |
| `parser/ini.h`        | INI parser built on the `parser/parser.h` interface | Prototype |
| `stream/stream.h`     | Generic byte stream interface.               | Prototype |
| `stream/arrstream.h`  | `stream/stream.h` implementation for C arrays. | Conceptual |
| `stream/fdstream.h`   | `stream/stream.h` implementation for Unix file descriptors. | Prototype |
| `table/table.h`       | Generic interface for associative arrays (aka hash tables (aka hash maps)) | Prototype |
| `table/hashtable.h`   | Bindings between `hash/hash.h` and `table/tableh` | Prototype |
| `container.h`         | Tools for dealing with object contexts/containers | Prototype |
| `ints.h`              | Integer typedefs that aren't stupid.         | Prototype |
| `macros.h`            | Helpful macros that don't fit anywhere else. | Prototype |

## Development Phases

I write code for `libcore` when one of my other projects would benefit from it. `libcore` development also occurs when someone else decides they want to contribute to it. Either way, `libcore` development works in these phases:

1. **Pre-conceptual**: Consider a new feature. Sketch it out on paper, or in some files. Don't put those files in the repo yet.
2. **Conceptual**: Design an interface that obeys the principles above. Put it into .h files.
3. **Prototype**: Implement the interfaces defined in the .h files from step 1. Test them thoroughly.
4. **Development**: Create a useful binary package that uses the interfaces at a small scale. Modify the code from step 3 as necessary.
5. **Production**: Ensure that the code from steps 3 and 4 is secure and bug-free. From here on out, API changes aren't allowed- only extensions to the existing API are permitted.
