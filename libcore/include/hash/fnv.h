/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"
#include "hash/hash.h"

int fnv1a_32_init(struct hash32 *hash32);

int fnv1a_64_init(struct hash64 *hash64);
