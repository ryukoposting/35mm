/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

struct hash {
  usize buf_size;
  int (*init)(void *result, struct hash const *hash);
  int (*digest)(void const *in, usize nbytes, void *result, struct hash const *hash);
};

struct hash32 {
  struct hash hash;
};

struct hash64 {
  struct hash hash;
};

#if HAS_INT128
struct hash128 {
  struct hash hash;
};
#endif

int hash_init(
  usize buf_size,
  int (*init)(void *result, struct hash const *hash),
  int (*digest)(void const *in, usize nbytes, void *result, struct hash const *hash),
  struct hash *hash);

int hash_start(void *result, usize result_len, struct hash const *hash);
int hash_digest(void const *in, usize nbytes, void *result, usize result_len, struct hash const *hash);

int hash32_start(uint32 *result, struct hash32 const *hash);
int hash32_digest(void const *in, usize nbytes, uint32 *result, struct hash32 const *hash);

int hash64_start(uint64 *result, struct hash64 const *hash);
int hash64_digest(void const *in, usize nbytes, uint64 *result, struct hash64 const *hash);

#if HAS_INT128
int hash128_start(uint128 *result, struct hash128 const *hash);
int hash128_digest(void const *in, usize nbytes, uint128 *result, struct hash128 const *hash);
#endif
