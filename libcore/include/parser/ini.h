/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "parser/types.h"
#include "stream/stream.h"
#include "mem/allocator.h"
#include <stdbool.h>

struct ini_parser {
  struct parser parser;
  struct allocator const *allocator;
  char *tmpbuf;
  usize tmpbuf_sz, max_line_len;
  usize nbytes;
  int state;
  bool run;
};

int ini_parser_init(
  struct istream const *stream,
  struct parser_receiver const *receiver,
  struct allocator const *allocator,
  struct ini_parser *parser);
