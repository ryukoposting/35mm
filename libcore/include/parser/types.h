/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "stream/stream.h"

struct parser;

enum parse_itemtype {
  PARSE_ERROR,
  PARSE_OBJ_START,
  PARSE_OBJ_END,
  PARSE_ARR_START,
  PARSE_ARR_END,
  PARSE_STR,
  PARSE_INT,
  PARSE_FLOAT,
  PARSE_NIL,
  PARSE_KEY,
};

struct parse_lineinfo {
  int line;
  int column;
};

struct parse_item {
  enum parse_itemtype type;
  struct parse_lineinfo lineinfo;
  char const *str, *msg;
};

struct parser_receiver {
  void (*receive)(struct parse_item const *item, struct parser const *parser);
};

struct parser {
  struct istream const *stream;
  struct parser_receiver const *receiver;
  int (*pause)(struct parser const *parser);
  int (*run)(struct parser const *parser);
};
