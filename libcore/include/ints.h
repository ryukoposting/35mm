/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stdint.h>

typedef int8_t int8;
typedef uint8_t uint8;

typedef int16_t int16;
typedef uint16_t uint16;

typedef int32_t int32;
typedef uint32_t uint32;

typedef int64_t int64;
typedef uint64_t uint64;

typedef long isize;
typedef unsigned long usize;


#define decode_i16_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((int16)buf_[0]) << 8) |\
      (uint16)buf_[1];\
  })

#define decode_u16_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint16)buf_[0]) << 8) |\
      (uint16)buf_[1];\
  })

#define decode_i16_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((int16)buf_[1]) << 8) |\
      (uint16)buf_[0];\
  })

#define decode_u16_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint16)buf_[1]) << 8) |\
      (uint16)buf_[0];\
  })


#define decode_i32_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((int32)buf_[0]) << 24) |\
     ((uint32)buf_[1] << 16) |\
     ((uint32)buf_[2] << 8) |\
      (uint32)buf_[3];\
  })

#define decode_u32_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint32)buf_[0]) << 24) |\
     ((uint32)buf_[1] << 16) |\
     ((uint32)buf_[2] << 8) |\
      (uint32)buf_[3];\
  })

#define decode_i32_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((int32)buf_[3]) << 24) |\
     ((uint32)buf_[2] << 16) |\
     ((uint32)buf_[1] << 8) |\
      (uint32)buf_[0];\
  })

#define decode_u32_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint32)buf_[3]) << 24) |\
     ((uint32)buf_[2] << 16) |\
     ((uint32)buf_[1] << 8) |\
      (uint32)buf_[0];\
  })


#define decode_i64_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((int64)buf_[0]) << 56) |\
     ((uint64)buf_[1] << 48) |\
     ((uint64)buf_[2] << 40) |\
     ((uint64)buf_[3] << 32) |\
     ((uint64)buf_[4] << 24) |\
     ((uint64)buf_[5] << 16) |\
     ((uint64)buf_[6] << 8) |\
      (uint64)buf_[7];\
  })

#define decode_u64_be(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint64)buf_[0]) << 56) |\
     ((uint64)buf_[1] << 48) |\
     ((uint64)buf_[2] << 40) |\
     ((uint64)buf_[3] << 32) |\
     ((uint64)buf_[4] << 24) |\
     ((uint64)buf_[5] << 16) |\
     ((uint64)buf_[6] << 8) |\
      (uint64)buf_[7];\
  })

#define decode_i64_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((int64)buf_[7]) << 56) |\
     ((uint64)buf_[6] << 48) |\
     ((uint64)buf_[5] << 40) |\
     ((uint64)buf_[4] << 32) |\
     ((uint64)buf_[3] << 24) |\
     ((uint64)buf_[2] << 16) |\
     ((uint64)buf_[1] << 8) |\
      (uint64)buf_[0];\
  })

#define decode_u64_le(buf) ({\
    uint8 *const buf_ = buf;\
    (((uint64)buf_[7]) << 56) |\
     ((uint64)buf_[6] << 48) |\
     ((uint64)buf_[5] << 40) |\
     ((uint64)buf_[4] << 32) |\
     ((uint64)buf_[3] << 24) |\
     ((uint64)buf_[2] << 16) |\
     ((uint64)buf_[1] << 8) |\
      (uint64)buf_[0];\
  })
