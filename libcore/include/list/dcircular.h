/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "list/types.h"

int dcllist_init(void *(*getter)(struct dcllnode const*), struct dcllist *dcllist);
int dcllnode_init(struct dcllnode *node);

int dcllist_push_front(struct dcllnode *node, struct dcllist *dcllist);
int dcllist_push_back(struct dcllnode *node, struct dcllist *dcllist);

void *dcllist_pop_front(struct dcllist *dcllist);
void *dcllist_pop_back(struct dcllist *dcllist);

int dcllist_rotate(int nrotate, struct dcllist *dcllist);
