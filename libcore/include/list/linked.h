/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "list/types.h"

#define llist_findit(llistp, cond) ({\
    typeof(llistp) _llistp = (llistp);\
    struct llnode *_lp = NULL;\
    for (_lp = _llistp->head; _lp; _lp = _lp->next) {\
      struct llnode const *const IT = _lp;\
      if (({ cond })) break;\
    }\
    _lp;\
  })

#define llist_eachit(llistp, action) ({\
    typeof(llistp) _llistp = (llistp);\
    struct llnode *_lp = NULL;\
    for (_lp = _llistp->head; _lp; _lp = _lp->next) {\
      struct llnode const *const IT = _lp;\
      do {action} while (0);\
    }\
    (NULL);\
  })

int llist_init(struct llist *llist);
int llnode_init(struct llnode *node);

int llist_push_front(struct llnode *node, struct llist *llist);
int llist_push_back(struct llnode *node, struct llist *llist);

struct llnode *llist_pop_front(struct llist *llist);
// void *llist_pop_back(struct llist *llist);

int llist_remove(struct llnode *node, struct llist *llist);

int llist_insert(struct llnode *node, usize pos, struct llist *llist);
