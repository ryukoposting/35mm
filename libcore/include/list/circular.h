/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "list/types.h"

int cllist_init(void *(*getter)(struct cllnode const*), struct cllist *cllist);
int cllnode_init(struct cllnode *node);

int cllist_push_front(struct cllnode *node, struct cllist *cllist);
// int cllist_push_back(struct cllnode *node, struct cllist *cllist);

void *cllist_pop_front(struct cllist *cllist);
// void *cllist_pop_back(struct cllist *cllist);

int cllist_rotate(int nrotate, struct cllist *cllist);
