/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

struct llist;
struct llnode;

struct dllist;
struct dllnode;

struct cllist;
struct cllnode;

struct dcllist;
struct dcllnode;


struct llist {
  void *(*getter)(struct llnode const*);
  struct llnode *head, **tailp;
  usize length;
};

struct llnode {
  struct llnode *next;
};


struct dllist {
  void *(*getter)(struct dllnode const*);
  struct dllnode *head, *tail;
  usize length;
};

struct dllnode {
  struct dllnode *next, *prev;
};


struct cllist {
  void *(*getter)(struct cllnode const*);
  struct cllnode *head;
  usize length;
};

struct cllnode {
  struct cllnode *next;
};


struct dcllist {
  void *(*getter)(struct dcllnode const*);
  struct dcllnode *head;
  usize length;
};

struct dcllnode {
  struct dcllnode *next, *prev;
};
