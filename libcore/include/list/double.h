/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "list/types.h"

int dllist_init(void *(*getter)(struct dllnode const*), struct dllist *dllist);
int dllnode_init(struct dllnode *node);

int dllist_push_front(struct dllnode *node, struct dllist *dllist);
int dllist_push_back(struct dllnode *node, struct dllist *dllist);

void *dllist_pop_front(struct dllist *dllist);
void *dllist_pop_back(struct dllist *dllist);
