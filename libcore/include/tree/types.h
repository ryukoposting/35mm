/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

struct bintree;
struct bintreenode;

struct bintree {
  void *(*getter)(struct bintreenode const*);
  struct bintreenode *root;
  usize max_depth;
  usize size;
};

struct bintreenode {
  struct bintreenode *left, *right;
};
