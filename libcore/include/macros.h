/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#define STRINGIFY_(X) # X
#define STRINGIFY(X) STRINGIFY_(X)

#define CONCAT_(X,Y) X ## Y
#define CONCAT(X,Y) CONCAT_(X,Y)

#define CONCAT3_(X,Y,Z) X ## Y ## Z
#define CONCAT3(X,Y,Z) CONCAT3_(X,Y,Z)

#define unused(v) ((void)v)

#define min(x,y) ({\
    typeof(x) x_ = (x);\
    typeof(y) y_ = (y);\
    (x_ < y_) ? x_ : y_;\
  })

#define min3(x,y,z) ({\
    typeof(x) x_ = (x);\
    typeof(y) y_ = (y);\
    typeof(z) z_ = (z);\
    (x_ < y_) ? (x_ < z_ ? x_ : z_) : (y_ < z_ ? y_ : z_);\
  })

#define max(x,y) ({\
    typeof(x) x_ = (x);\
    typeof(y) y_ = (y);\
    (x_ > y_) ? x_ : y_;\
  })

#define max3(x,y,z) ({\
    typeof(x) x_ = (x);\
    typeof(y) y_ = (y);\
    typeof(z) z_ = (z);\
    (x_ > y_) ? (x_ > z_ ? x_ : z_) : (y_ > z_ ? y_ : z_);\
  })
