/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "mem/allocator.h"
#include "mem/rc.h"

struct alrc {
  struct rctype rctype;
  struct allocator const *allocator;
};

int alrc_init(struct allocator const *allocator, struct alrc *alrc);

void *alrc_alloc(usize nbytes, void (*on_release)(void*), struct alrc *alrc);
int alrc_get(void *p);
int alrc_put(void *p);
// void *alrc_reallocate(void *p, usize nbytes);
