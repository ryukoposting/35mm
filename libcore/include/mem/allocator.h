/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stddef.h>
#include "ints.h"

struct allocator {
  void *(*alloc)(usize nbytes, struct allocator const *allocator);
  void *(*realloc)(void *p, usize nbytes, struct allocator const *allocator);
  void (*free)(void *p, struct allocator const *allocator);
};

static inline void *allocate(usize nbytes, struct allocator const *allocator)
{
  if (!allocator || nbytes == 0 || !allocator->alloc)
    return NULL;
  else
    return allocator->alloc(nbytes, allocator);
}

static inline void release(void *p, struct allocator const *allocator)
{
  if (allocator && allocator->free)
    allocator->free(p, allocator);
}

static inline void *reallocate(void *p, usize nbytes, struct allocator const *allocator)
{
  if (nbytes == 0) {
    release(p, allocator);
    return NULL;
  } else if (!allocator || !allocator->realloc) {
    return NULL;
  } else {
    return allocator->realloc(p, nbytes, allocator);
  }
}
