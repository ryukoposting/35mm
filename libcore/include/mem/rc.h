/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

struct rc;
struct rctype;

struct rc {
  struct rctype const *rctype;
  isize count;
};

struct rctype {
  void (*get)(struct rc const *rc, isize count);
  void (*put)(struct rc const *rc, isize count);
};


int rctype_init(
  void (*get)(struct rc const *rc, isize count),
  void (*put)(struct rc const *rc, isize count),
  struct rctype *rctype);


int rc_init(struct rctype const *rctype, struct rc *rc);

int rc_get(struct rc *rc);

int rc_put(struct rc *rc);
