/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "stream/stream.h"

struct aistream {
  void const *p;
  usize ofsp, sz;
  struct istream istream;
};

struct aostream {
  void *p;
  usize ofsp, sz;
  struct ostream ostream;
};

int aistream_init(void const *bufp, usize length, struct aistream *stream);

int aostream_init(void *bufp, usize length, struct aostream *stream);
