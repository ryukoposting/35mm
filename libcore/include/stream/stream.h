/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

#define stream_read(buf, count, stream) (_Generic((*stream),\
    struct istream: !stream->read ? -1 : stream->read(buf, count, stream),\
    struct iostream: !stream->read ? -1 : stream->read(buf, count, stream)\
  ))

#define stream_bread(buf, count, retry, stream) (_Generic((*stream),\
    struct istream: istream_bread(buf, count, retry, stream),\
    struct iostream: iostream_bread(buf, count, retry, stream)\
  ))

#define stream_write(buf, count, stream) (_Generic((*stream),\
    struct ostream: !stream->write ? -1 : stream->write(buf, count, stream),\
    struct iostream: !stream->write ? -1 : stream->write(buf, count, stream)\
  ))

#define stream_bwrite(buf, count, retry, stream) (_Generic((*stream),\
    struct ostream: ostream_bwrite(buf, count, retry, stream),\
    struct iostream: iostream_bwrite(buf, count, retry, stream)\
  ))

#define stream_seek(off, whence, stream) (_Generic((*stream),\
    struct istream: !stream->seek ? -1 : stream->seek(off, whence, stream),\
    struct ostream: !stream->seek ? -1 : stream->seek(off, whence, stream),\
    struct iostream: !stream->seek ? -1 : stream->seek(off, whence, stream)\
  ))

struct istream;
struct ostream;
struct iostream;

enum whence {
  WSET = 0,
  WCUR = 1,
  WEND = 2,
};

struct istream {
  isize (*read)(void *buf, usize count, struct istream const *istream);
  isize (*seek)(long off, enum whence whence, struct istream const *istream);
};

struct ostream {
  isize (*write)(void const *buf, usize count, struct ostream const *ostream);
  isize (*seek)(long off, enum whence whence, struct ostream const *ostream);
};

struct iostream {
  isize (*read)(void *buf, usize count, struct iostream const *iostream);
  isize (*write)(void const *buf, usize count, struct iostream const *iostream);
  isize (*seek)(long off, enum whence whence, struct iostream const *iostream);
};

isize istream_bread(void *buf, isize count, int retry, struct istream const *istream);

isize ostream_bwrite(void const *buf, isize count, int retry, struct ostream const *ostream);

isize iostream_bread(void *buf, isize count, int retry, struct iostream const *iostream);

isize iostream_bwrite(void const *buf, isize count, int retry, struct ostream const *iostream);
