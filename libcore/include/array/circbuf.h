/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stddef.h>
#include "mem/allocator.h"

struct circbuf;

struct circbuf {
  struct allocator const *allocator;
  usize elemsz, nelems;
  usize start, len;
  void *bufp;
};

int circbuf_init_raw(
  usize elemsz,
  usize nelems,
  void *buf,
  struct allocator const *allocator,
  struct circbuf *circbuf);

int circbuf_push_back(void const *obj, struct circbuf *circbuf);

int circbuf_push_front(void const *obj, struct circbuf *circbuf);

int circbuf_pop_back(void *obj, struct circbuf *circbuf);

int circbuf_pop_front(void *obj, struct circbuf *circbuf);
