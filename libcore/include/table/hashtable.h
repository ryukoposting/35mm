/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "hash/hash.h"
#include "mem/allocator.h"
#include "table/table.h"
#include "list/linked.h"

struct hashtable_type {
  struct allocator const *allocator;
  struct hash64 const *hash;
};

struct hashtable {
  struct hashtable_type const *type;
  struct table table;
  struct llist *data;
  usize ldata;
  usize nelems;
};

int hashtable_type_init(
  struct allocator const *allocator,
  struct hash64 const *hash,
  struct hashtable_type *type);

int hashtable_init(
  struct hashtable_type const *type,
  struct hashtable *table);
