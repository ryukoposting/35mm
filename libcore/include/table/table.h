/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"

struct table {
  void *(*get)(void const *key, usize keysize, struct table const *table);
  void *(*remove_key)(void const *key, usize keysize, struct table const *table);
  int (*put)(void const *key, usize keysize, void *value, struct table const *table);
};

int table_init(
  void *(*get)(void const *key, usize keysize, struct table const *table),
  int (*put)(void const *key, usize keysize, void *value, struct table const *table),
  void *(*remove_key)(void const *key, usize keysize, struct table const *table),
  struct table *table);

int table_delete(struct table *table);

void *table_get(void const *key, usize keysize, struct table const *table);

int table_put(void const *key, usize keysize, void *value, struct table const *table);

void *table_remove(void const *key, usize keysize, struct table const *table);
