/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "parself/types.h"

#define ELF_READER_MAXPUSH 4

struct elf_reader {
  struct istream const *istream;
  struct elf_hdr hdr;

  isize offs[ELF_READER_MAXPUSH+1];
  int offsp;

  usize err_at;
  int err_cause;
};

int elf_read_start(struct istream *istream, struct elf_reader *reader);


int elf_read_push(struct elf_reader *reader);

int elf_read_pop(struct elf_reader *reader);


int elf_read_phdr(struct elf_phdr *phdr, int entno, struct elf_reader *reader);

int elf_next_phdr(struct elf_phdr *phdr, struct elf_reader *reader);


int elf_read_shdr(struct elf_phdr *phdr, int entno, struct elf_reader *reader);

int elf_next_shdr(struct elf_phdr *phdr, struct elf_reader *reader);

