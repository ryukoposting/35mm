/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include "ints.h"
#include "stream/stream.h"

#define elf_hdr_is_elf32(hdrp) ((hdrp)->base[EI_CLASS] == 1)
#define elf_hdr_is_elf64(hdrp) ((hdrp)->base[EI_CLASS] == 2)

#define elf_hdr_is_le(hdrp) ((hdrp)->base[EI_DATA] == 1)
#define elf_hdr_is_be(hdrp) ((hdrp)->base[EI_DATA] == 2)

#define elf_hdr_entry(hdrp) (elf_hdr_is_elf32(hdrp) ? (hdrp)->e_entry : (hdrp)->e_entry_64)
#define elf_hdr_phoff(hdrp) (elf_hdr_is_elf32(hdrp) ? (hdrp)->e_phoff : (hdrp)->e_phoff_64)
#define elf_hdr_shoff(hdrp) (elf_hdr_is_elf32(hdrp) ? (hdrp)->e_shoff : (hdrp)->e_shoff)

#define elf_shdr_flags(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_flags : (shdrp)->sh_flags_64)
#define elf_shdr_addr(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_addr : (shdrp)->sh_addr_64)
#define elf_shdr_off(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_offset : (shdrp)->sh_offset_64)
#define elf_shdr_size(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_size : (shdrp)->sh_size_64)
#define elf_shdr_addralign(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_addralign : (shdrp)->sh_addralign_64)
#define elf_shdr_entsize(hdrp, shdrp) (elf_hdr_is_elf32(hdrp) ? (shdrp)->sh_entsize : (shdrp)->sh_entsize_64)

struct elf_hdr;
struct elf_phdr;
struct elf_shdr;

enum elf_hdr_base {
  EI_MAG0,
  EI_MAG1,
  EI_MAG2,
  EI_MAG3,
  EI_MAG_LEN,
  EI_CLASS = EI_MAG_LEN,
  EI_DATA,
  EI_VERSION,
  EI_OSABI,
  EI_ABIVERSION,
  ELF_HDR_BASE_LEN
};

enum elf_e_type {
  ET_NONE = (uint16)0x00,
  ET_REL = (uint16)0x01,
  ET_EXEC = (uint16)0x02,
  ET_DYN = (uint16)0x03,
  ET_CORE = (uint16)0x04,
  ET_LOOS = (uint16)0xfe00,
  ET_HIOS = (uint16)0xfeff,
  ET_LOPROC = (uint16)0xff00,
  ET_HIPROC = (uint16)0xffff,
};

enum elf_e_machine {
  EM_ARM = (uint16)0x28,
  EM_ARM64 = (uint16)0xb7
};

struct elf_hdr {
  char base[ELF_HDR_BASE_LEN];
  uint16 e_type;
  uint16 e_machine;
  uint32 e_version;
  union {
    uint32 e_entry;
    uint64 e_entry_64;
  };
  union {
    uint32 e_phoff;
    uint64 e_phoff_64;
  };
  union {
    uint32 e_shoff;
    uint64 e_shoff_64;
  };
  uint32 e_flags;
  uint16 e_ehsize;
  uint16 e_phentsize;
  uint16 e_phnum;
  uint16 e_shentsize;
  uint16 e_shnum;
  uint16 e_shstrndx;
};

struct elf_phdr {
  uint32 p_type;
  union {
    uint32 p_offset;
    uint64 p_offset_64;
  };
  union {
    uint32 p_vaddr;
    uint64 p_vaddr_64;
  };
  union {
    uint32 p_paddr;
    uint64 p_paddr_64;
  };
  union {
    uint32 p_filesz;
    uint64 p_filesz_64;
  };
  union {
    uint32 p_memsz;
    uint64 p_memsz_64;
  };
  uint32 p_flags;
  union {
    uint32 p_align;
    uint64 p_align_64;
  };
};

struct elf_shdr {
  uint32 sh_name;
  uint32 sh_type;
  union {
    uint32 sh_flags;
    uint64 sh_flags_64;
  };
  union {
    uint32 sh_addr;
    uint64 sh_addr_64;
  };
  union {
    uint32 sh_offset;
    uint64 sh_offset_64;
  };
  union {
    uint32 sh_size;
    uint64 sh_size_64;
  };
  uint32 sh_link;
  uint32 sh_info;
  union {
    uint32 sh_addralign;
    uint64 sh_addralign_64;
  };
  union {
    uint32 sh_entsize;
    uint64 sh_entsize_64;
  };
};

struct elf_sym {
  uint32 st_name;
  union {
    uint32 st_value;
    uint64 st_value_64;
  };
  union {
    uint32 st_size;
    uint64 st_size_64;
  };
  char st_info;
  char st_other;
  uint16 st_shndx;
};
