/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stddef.h>

#define container_of(ptr, type, member) ({\
    const typeof(((type*)NULL)->member) *mptr_ = (ptr);\
    (type*)((char*)mptr_ - offsetof(type, member));\
  })
